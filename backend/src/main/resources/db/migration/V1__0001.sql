CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE user_acc
(
    id                         serial primary key,
    username                   varchar(100) UNIQUE,
    email                      varchar(256) UNIQUE,
    password                   varchar(100),
    is_account_non_expired     boolean,
    is_account_non_locked      boolean,
    is_credentials_non_expired boolean,
    is_enabled                 boolean,
    budget_id                  bigint
);

CREATE TABLE budget
(
    id          serial primary key,
    name        varchar(50),
    description varchar(50),
    first_day   smallint,
    currency    varchar(5),
    user_id     bigint,
    deal_id     bigint
);

VALUES ('michal', 'michal@mail.com', '$2a$10$49QYxI1CPIU468FvfYQqt.4.5jaekNyhULPOzeiKz1JbJWLXEFrN.', '1', '1', '1',
        '1');

CREATE TABLE deal
(
    id          serial primary key,
    deal_type   varchar(20),
    deal_source varchar(20),
    note        varchar(200),
    value       decimal,
    datetime    timestamptz,
    budget_id   bigint,
    photo_id    bigint,
    latitude    varchar(20),
    longitude   varchar(20)
);

CREATE TABLE file
(
    id       serial primary key,
    filename varchar(100),
    filetype varchar(100),
    filedata bytea
);

CREATE TABLE limitation
(
    id            serial primary key,
    value         decimal,
    limit_type_id bigint,
    from_when     timestamptz,
    until         timestamptz,
    budget_id     bigint
);

CREATE TABLE limit_type
(
    id   serial primary key,
    type varchar(50)
);

ALTER TABLE user_acc
    ADD CONSTRAINT constraint_budget_fk FOREIGN KEY (budget_id) REFERENCES budget (id);

ALTER TABLE budget
    ADD CONSTRAINT constraint_user_fk FOREIGN KEY (user_id) REFERENCES user_acc (id);

ALTER TABLE budget
    ADD CONSTRAINT constraint_deal_fk FOREIGN KEY (deal_id) REFERENCES deal (id);

ALTER TABLE deal
    ADD CONSTRAINT constraint_budget_fk FOREIGN KEY (budget_id) REFERENCES budget (id);

ALTER TABLE limitation
    ADD CONSTRAINT constraing_budget_fk FOREIGN KEY (budget_id) references budget (id);

INSERT INTO user_acc (username,
                      email,
                      password,
                      is_account_non_expired,
                      is_account_non_locked,
                      is_credentials_non_expired,
                      is_enabled)
VALUES ('michal', 'michal@mail.com', '$2a$10$49QYxI1CPIU468FvfYQqt.4.5jaekNyhULPOzeiKz1JbJWLXEFrN.', '1', '1', '1',
        '1');

INSERT INTO user_acc (username,
                      email,
                      password,
                      is_account_non_expired,
                      is_account_non_locked,
                      is_credentials_non_expired,
                      is_enabled)
VALUES ('michall', 'michall@mail.com', '$2a$10$49QYxI1CPIU468FvfYQqt.4.5jaekNyhULPOzeiKz1JbJWLXEFrN.', '1', '1', '1',
        '1');

INSERT INTO budget (name,
                    description,
                    first_day,
                    currency,
                    user_id,
                    deal_id)
VALUES ('daily', 'na codzienne wydatki', 1, 'PLN', 1, null);

INSERT INTO limit_type(type) VALUES ('SAVE_MONEY');
INSERT INTO limit_type(type) VALUES ('SPEND_NOT_MORE_THAN');
