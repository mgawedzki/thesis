package com.gawedzki.backend.application.limit;

import com.gawedzki.backend.application.budget.Budget;
import com.gawedzki.backend.application.limit.limittype.LimitType;
import com.gawedzki.backend.application.user.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Getter
@Setter
@Table(name = "limitation")
public class Limit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Float value;
    @OneToOne
    private LimitType limitType;
    @Column(name = "from_when")
    private Instant from;
    private Instant until;
    @ManyToOne
    private Budget budget;
}
