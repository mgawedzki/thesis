package com.gawedzki.backend.application.budget;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.gawedzki.backend.application.deal.Deal;
import com.gawedzki.backend.application.limit.Limit;
import com.gawedzki.backend.application.user.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.*;

@Getter
@Setter
@Entity
public class Budget {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;
    private int firstDay;
    @ManyToOne
    private User user;
    private Currency currency;
    @JsonBackReference
    @OneToMany(mappedBy = "budget", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Deal> deals = new HashSet<>();
    @OneToMany(mappedBy = "budget", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Limit> limits;
}
