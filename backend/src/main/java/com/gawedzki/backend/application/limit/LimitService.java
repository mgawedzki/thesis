package com.gawedzki.backend.application.limit;

import com.gawedzki.backend.application.budget.Budget;
import com.gawedzki.backend.application.budget.BudgetRepository;
import com.gawedzki.backend.application.limit.limittype.LimitType;
import com.gawedzki.backend.application.limit.limittype.LimitTypeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LimitService {

    private final LimitRepository limitRepository;
    private final BudgetRepository budgetRepository;
    private final LimitTypeRepository limitTypeRepository;

    public LimitService(LimitRepository limitRepository, BudgetRepository budgetRepository, LimitTypeRepository limitTypeRepository) {
        this.limitRepository = limitRepository;
        this.budgetRepository = budgetRepository;
        this.limitTypeRepository = limitTypeRepository;
    }

    public List<Limit> getAll() {
        return limitRepository.findAll();
    }

    public Limit save(LimitRequest request) {
        Limit limit = new Limit();
        Budget budget = budgetRepository.findById(request.getBudgetId()).orElseThrow(() -> new RuntimeException("wrong budget id"));
        LimitType limitType = limitTypeRepository.findById(request.getLimitTypeId()).orElseThrow(() -> new RuntimeException("wrong limittype id"));

        limit.setBudget(budget);
        limit.setFrom(request.getFrom());
        limit.setUntil(request.getUntil());
        limit.setValue(request.getValue());
        limit.setLimitType(limitType);

        return limitRepository.save(limit);
    }

    public void delete(Long id) {
        limitRepository.deleteById(id);
    }
}
