package com.gawedzki.backend.application.deal;

import com.gawedzki.backend.application.budget.Budget;
import com.gawedzki.backend.application.budget.BudgetRepository;
import com.gawedzki.backend.application.file.File;
import com.gawedzki.backend.application.user.UserRepository;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class DealService {

    private final DealRepository dealRepository;
    private final UserRepository userRepository;
    private final BudgetRepository budgetRepository;

    public DealService(DealRepository dealRepository, UserRepository userRepository, BudgetRepository budgetRepository) {
        this.dealRepository = dealRepository;
        this.userRepository = userRepository;
        this.budgetRepository = budgetRepository;
    }

    public Deal save(DealRequest dealRequest) throws IOException {

        Budget budget;
//        User user = userRepository.findById(user_id).orElseThrow(() -> new RuntimeException("User not found"));
        if (dealRequest.budgetId != null)
            budget = budgetRepository.getOne(dealRequest.budgetId);
        else if (dealRequest.budgetName != null)
            budget = budgetRepository.findByName(dealRequest.budgetName);
        else throw new RuntimeException("Couldn't find deal");

        if (dealRequest.datetime == null) dealRequest.datetime = Instant.now();

        String filename = null;
        File file = null;
        if (dealRequest.getPhoto() != null) {
            filename = StringUtils.cleanPath(dealRequest.getPhoto().getOriginalFilename());

            file = new File(
                    filename,
                    dealRequest.getPhoto().getContentType(),
                    dealRequest.getPhoto().getBytes());
        }

        Deal deal = new Deal(
                dealRequest.dealtype,
                dealRequest.dealsource,
                dealRequest.note,
                dealRequest.value,
                dealRequest.datetime,
                new Location(dealRequest.latitude, dealRequest.longitude),
                budget,
                file
        );

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
        System.out.println(LocalDateTime.now().format(formatter) + "  DealService: save()");

        return dealRepository.save(deal);
    }

    public Deal update(Long id, DealRequest request) throws IOException {
        Deal deal = dealRepository.findById(id).orElseThrow();

        if(request.budgetId != null) {
            Budget budget = budgetRepository.getOne(request.budgetId);
            deal.setBudget(budget);
        }
        if (request.note != null)
            deal.setNote(request.note);
        if (request.value != null)
            deal.setValue(request.value);
        if (request.dealtype != null)
            deal.setDealType(request.dealtype);
        if (request.dealsource != null)
            deal.setDealSource(request.dealsource);
        if (request.photo != null) {
            String filename = null;
            File file = null;
            if (request.getPhoto() != null) {
                filename = StringUtils.cleanPath(request.getPhoto().getOriginalFilename());

                file = new File(
                        filename,
                        request.getPhoto().getContentType(),
                        request.getPhoto().getBytes());
            }
            deal.setPhoto(file);
        }
        return dealRepository.save(deal);
    }

    public Deal getDealById(Long id) {
        return dealRepository.findById(id).get();
    }

    public List<Deal> getMyDeals(Long id) {
        Sort sort = Sort.by(Sort.Order.desc("datetime"));
        return dealRepository.findAllByBudget_User_Id(id, sort);
    }

    public void delete(Long id) {
        dealRepository.deleteById(id);
    }
}
