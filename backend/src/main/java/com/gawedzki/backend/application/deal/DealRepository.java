package com.gawedzki.backend.application.deal;

import com.gawedzki.backend.application.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface DealRepository extends JpaRepository<Deal, Long> {
    List<Deal> findAllByBudget_User_Id(Long id, Sort sort);
//    List<Deal> findAllByBudget_User_IdO(Long id);
}
