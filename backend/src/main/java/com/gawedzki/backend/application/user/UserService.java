package com.gawedzki.backend.application.user;

import com.gawedzki.backend.application.budget.Budget;
import com.gawedzki.backend.application.budget.BudgetRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Currency;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final BudgetRepository budgetRepository;

    public UserService(@Qualifier("postgres") UserRepository userRepository, PasswordEncoder passwordEncoder, BudgetRepository budgetRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.budgetRepository = budgetRepository;
    }

    public List<UserDto> findAll() {
        return userRepository.findAll().stream()
                .map(UserDto::from).collect(Collectors.toList());
    }

    public UserDto create(UserRequest request) {

        User user;

//        Optional<User> userByEmail = userRepository.findByEmail(request.getEmail());
        Optional<User> userByUsername = userRepository.findByUsername(request.getUsername());

        if (userByUsername.isPresent()) {
            throw new RuntimeException("This username is already in use");
        }
//        if (userByEmail.isPresent()) {
//            throw new RuntimeException("This email is already in use");
//        }


        user = new User(
                request.getUsername(),
                request.getEmail(),
                passwordEncoder.encode(request.getPassword()),
                true,
                true,
                true,
                true
        );
        userRepository.save(user);
        UserDto dto = UserDto.from(user);

        Budget budget = new Budget();
        budget.setName("No. 1");
        budget.setFirstDay(1);
        Currency currency = Currency.getInstance("PLN");
        budget.setCurrency(currency);
        budget.setUser(user);
        budgetRepository.save(budget);

        return dto;

    }

    public UserDto getById(Long id) {
        return UserDto.from(userRepository.getOne(id));
    }

    public UserDto getMe(String token) {
        return null;
    }

    public UserDto getByToken(String username) {
        User user = userRepository.findByUsername(username).orElseThrow(() -> new RuntimeException("User not found"));
        return UserDto.from(user);
    }
}
