package com.gawedzki.backend.application.budget;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.time.Instant;
import java.util.List;

public interface BudgetRepository extends JpaRepository<Budget, Long>, JpaSpecificationExecutor<Budget> {
    List<Budget> findAllByUser_Id(Long id);
    Budget findByUser_IdAndNameIgnoreCase(Long id, String name);
    Budget findByName(String name);
    Budget getByName(String name);

    @Query("select sum(d.value) from Deal d where (d.budget.id=?1 and d.datetime >= ?2)")
    Double getLastNDaysBalance(Long budgetId, Instant timestamp);
}
