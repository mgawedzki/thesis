package com.gawedzki.backend.application.file;

import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@Entity
public class File {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String filename;
    private String filetype;
    @Lob
    @Type(type = "org.hibernate.type.BinaryType")
    private byte[] filedata;

    public File() {

    }

    public File(String filename, String filetype, byte[] filedata) {
        this.filename = filename;
        this.filetype = filetype;
        this.filedata = filedata;
    }
}
