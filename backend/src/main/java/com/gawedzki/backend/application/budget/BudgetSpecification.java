package com.gawedzki.backend.application.budget;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Path;

public class BudgetSpecification {

    public static Specification<Budget> nameEquals(String name) {
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.like(root.get("name"), name);
    }

    public static Specification<Budget> userIdEquals(Long userId){
        return (root, criteriaQuery, criteriaBuilder) -> {
            Path<Object> path = root.join("user").get("id");
            return criteriaBuilder.equal(path, userId);
        };
    }
}
