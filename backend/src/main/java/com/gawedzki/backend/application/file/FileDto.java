package com.gawedzki.backend.application.file;

import com.gawedzki.backend.application.deal.Deal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@Data
@Builder
@AllArgsConstructor
public class FileDto {
    private String filename;
    private String url;
    private String filetype;
    private long size;

    public FileDto() {
    }

    public static FileDto from(Deal deal) {
        if(deal.getPhoto() == null) return null;
        FileDto dto = new FileDto();

        File photo = deal.getPhoto();
        if (photo != null) {
            String fileDownloadUri = ServletUriComponentsBuilder
                    .fromCurrentContextPath()
                    .path("/deals/files/")
                    .path(deal.getId().toString())
                    .toUriString();

            dto.filename = photo.getFilename();
            dto.filetype = photo.getFiletype();
            dto.size = photo.getFiledata().length;
            dto.url = fileDownloadUri;
        }
        return dto;
    }
}


