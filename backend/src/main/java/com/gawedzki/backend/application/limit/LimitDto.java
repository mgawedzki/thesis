package com.gawedzki.backend.application.limit;

import com.gawedzki.backend.application.budget.BudgetDto;
import com.gawedzki.backend.application.deal.Deal;
import com.gawedzki.backend.application.limit.limittype.LimitType;
import com.gawedzki.backend.application.limit.limittype.LimitTypeEnum;
import lombok.Data;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class LimitDto {

    private Long id;
    private Float goalValue;
    private LimitType limitType;
    private Instant from;
    private Instant until;
    private BudgetDto budget;
    private float balance;

    public static LimitDto from(Limit limit) {
        LimitDto dto = new LimitDto();
        dto.id = limit.getId();
        dto.goalValue = limit.getValue();
        dto.limitType = limit.getLimitType();
        dto.from = limit.getFrom();
        dto.until = limit.getUntil();
        dto.budget = BudgetDto.from(limit.getBudget());

        return dto;
    }

    public static LimitDto from(Limit limit, List<Deal> deals) {
        LimitDto dto = new LimitDto();
        dto.id = limit.getId();
        dto.goalValue = limit.getValue();
        dto.limitType = limit.getLimitType();
        dto.from = limit.getFrom();
        dto.until = limit.getUntil();
        dto.budget = BudgetDto.from(limit.getBudget());
        dto.balance = calculateBalance(limit, deals);

        return dto;
    }

    private static float calculateBalance(Limit limit, List<Deal> deals) {
        Instant from = limit.getFrom();
        Instant until = limit.getUntil();
        deals = deals.stream()
                .filter(deal -> deal.getDatetime().isAfter(from))
                .filter(deal -> deal.getDatetime().isBefore(until.plus(1, ChronoUnit.DAYS)))
                .collect(Collectors.toList());

        float balance;
        if (limit.getLimitType().getType().equals(LimitTypeEnum.SAVE_MONEY))
            balance = (float) deals.stream().mapToDouble(Deal::getSignedValue).sum();
        else balance = (float) deals.stream().mapToDouble(Deal::getIfExpenseValue).sum();

        return balance;
    }
}
