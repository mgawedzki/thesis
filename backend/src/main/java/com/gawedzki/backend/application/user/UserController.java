package com.gawedzki.backend.application.user;

import com.gawedzki.backend.application.security.AppUserDetails;
import com.gawedzki.backend.application.security.CurrentUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public List<UserDto> findAll(@CurrentUser AppUserDetails user){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
        System.out.println(LocalDateTime.now().format(formatter) + "  UserService: findAll()");
        List<UserDto> all = userService.findAll();
        return all;
    }

    @PostMapping("/register")
    public UserDto createUser(@RequestBody UserRequest request){
        return userService.create(request);
    }

    @GetMapping("/{id}")
    public UserDto getById(@PathVariable Long id){
        return userService.getById(id);
    }

    @GetMapping("/me")
    public UserDto getByToken(@CurrentUser AppUserDetails user){

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
        System.out.println(LocalDateTime.now().format(formatter) + "  UserService: me()");
        UserDto byToken = userService.getByToken(user.getUsername());
        return byToken;
    }
}
