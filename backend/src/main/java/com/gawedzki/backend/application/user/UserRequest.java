package com.gawedzki.backend.application.user;

import lombok.Data;

import javax.persistence.Column;

@Data
public class UserRequest {
    private String username;
    private String email;
    private String password;
}
