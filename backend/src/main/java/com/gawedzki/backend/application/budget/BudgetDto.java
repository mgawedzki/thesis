package com.gawedzki.backend.application.budget;

import com.gawedzki.backend.application.deal.Deal;
import com.gawedzki.backend.application.deal.DealDto;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Transient;
import java.util.Currency;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
public class BudgetDto {

    public Long id;
    public String name;
    public String description;
    public int firstDay;
    public Currency currency;
    public List<DealDto> deals;

    public static BudgetDto from(Budget budget) {
        BudgetDto dto = new BudgetDto();
        dto.id = budget.getId();
        dto.name = budget.getName();
        dto.description = budget.getDescription();
        dto.firstDay = budget.getFirstDay();
        dto.currency = budget.getCurrency();
        dto.deals = budget.getDeals().stream().map(DealDto::from).collect(Collectors.toList());
        return dto;
    }

    public static BudgetDto excludeDeals(Budget budget) {
        BudgetDto budgetDto = new BudgetDto();
        budgetDto.id = budget.getId();
        budgetDto.name = budget.getName();
        budgetDto.currency = budget.getCurrency();
        budgetDto.description = budget.getDescription();
        budgetDto.firstDay = budget.getFirstDay();
        return budgetDto;
    }

}
