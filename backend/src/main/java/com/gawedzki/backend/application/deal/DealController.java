package com.gawedzki.backend.application.deal;

import com.gawedzki.backend.application.security.AppUserDetails;
import com.gawedzki.backend.application.security.CurrentUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("deals")
public class DealController {

    private final DealService dealService;

    public DealController(DealService dealService) {
        this.dealService = dealService;
    }

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public DealDto save(@CurrentUser AppUserDetails userDetails, DealRequest dealRequest) throws IOException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
        System.out.println(LocalDateTime.now().format(formatter) + "  DealController: save()");
        return DealDto.from(dealService.save(dealRequest));
    }

    @PutMapping(value = "/{id}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public DealDto update(@CurrentUser AppUserDetails userDetails, DealRequest dealRequest, @PathVariable Long id) throws IOException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
        System.out.println(LocalDateTime.now().format(formatter) + "  DealController: update()");
        return DealDto.from(dealService.update(id, dealRequest));
    }

    @GetMapping
    public List<DealDto> list(@CurrentUser AppUserDetails userDetails) {
        Long user_id = userDetails.getId();
        return dealService.getMyDeals(user_id)
                .stream()
                .map(DealDto::from)
                .collect(Collectors.toList());
    }

    @GetMapping("/files/{id}")
    public ResponseEntity<byte[]> getFile(@PathVariable Long id) {
        Deal deal = dealService.getDealById(id);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
        System.out.println(LocalDateTime.now().format(formatter) + "  DealController: getFile()");
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=\"" + deal.getPhoto().getFilename() + "\"")
                .body(deal.getPhoto().getFiledata());
    }

    @DeleteMapping("/{id}")
    public void delete(@CurrentUser AppUserDetails userDetails, @PathVariable Long id){
        dealService.delete(id);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
        System.out.println(LocalDateTime.now().format(formatter) + "  DealController: delete()");
    }
}
