package com.gawedzki.backend.application.security.jwt;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties("app.environments.jwt")
public class JwtConfig {
    private String secret;
    private int expirationAfterDays;
    private int expirationAfterSeconds;
    private int expirationAfterMonths;
}
