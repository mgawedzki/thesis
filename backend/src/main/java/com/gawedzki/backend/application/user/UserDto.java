package com.gawedzki.backend.application.user;

import com.gawedzki.backend.application.budget.BudgetDto;

import java.util.Set;
import java.util.stream.Collectors;

public class UserDto {

    public Long id; //fixme id niepotrzebne
    public String username;
    public String email;
//    public Set<BudgetDto> budgets;

    public static UserDto from(User user) {
        UserDto dto = new UserDto();
        dto.id = user.getId();
        dto.username = user.getUsername();
        dto.email = user.getEmail();
//        dto.budgets = user.getBudgets().stream().map(BudgetDto::from).collect(Collectors.toSet());
        return dto;
    }
}
