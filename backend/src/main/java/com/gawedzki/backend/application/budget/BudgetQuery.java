package com.gawedzki.backend.application.budget;

import com.gawedzki.backend.application.user.User;
import lombok.Data;

@Data
public class BudgetQuery {

    public Long userId;
    public String name;
    public String description;
    public Integer firstDay;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getFirstDay() {
        return firstDay;
    }

    public void setFirstDay(Integer firstDay) {
        this.firstDay = firstDay;
    }
}
