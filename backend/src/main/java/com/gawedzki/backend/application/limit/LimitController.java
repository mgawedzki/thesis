package com.gawedzki.backend.application.limit;

import com.gawedzki.backend.application.deal.DealRepository;
import com.gawedzki.backend.application.security.AppUserDetails;
import com.gawedzki.backend.application.security.CurrentUser;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("limits")
public class LimitController {

    private final LimitService limitService;
    private final DealRepository dealRepository;

    public LimitController(LimitService limitService, DealRepository dealRepository) {
        this.limitService = limitService;
        this.dealRepository = dealRepository;
    }


    @GetMapping("/")
    public List<LimitDto> getAll(@CurrentUser AppUserDetails userDetails) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
        System.out.println(LocalDateTime.now().format(formatter) + "  LimitController: getAll()");
        return limitService.getAll()
                .stream()
                .map(limit -> {
                    LimitDto dto = LimitDto.from(
                            limit,
                            dealRepository.findAllByBudget_User_Id(userDetails.getId(), Sort.by(Sort.Order.desc("datetime"))));
                    return dto;
                })
                .collect(Collectors.toList());
    }

    @PostMapping("/")
    public LimitDto save(@CurrentUser AppUserDetails userDetails, @RequestBody LimitRequest request) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
        System.out.println(LocalDateTime.now().format(formatter) + "  LimitController: save()");
        return LimitDto.from(
                limitService.save(request),
                dealRepository.findAllByBudget_User_Id(userDetails.getId(), Sort.by(Sort.Order.desc("datetime"))));
    }

    @DeleteMapping("/{id}")
    public void delete(@CurrentUser AppUserDetails userDetails, @PathVariable Long id) {
        limitService.delete(id);
    }
}
