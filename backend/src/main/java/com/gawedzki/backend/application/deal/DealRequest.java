package com.gawedzki.backend.application.deal;

import com.gawedzki.backend.application.deal.dealsource.DealSource;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.time.Instant;
import java.time.LocalDateTime;

@Data
public class DealRequest {
    public Long budgetId;
    public String budgetName;
    public DealType dealtype;
    public DealSource dealsource;
    public String note;
    public Float value;
    public Instant datetime;
    public MultipartFile photo;
    public Double latitude;
    public Double longitude;
    public String filePath;
}
