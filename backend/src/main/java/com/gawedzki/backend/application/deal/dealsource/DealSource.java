package com.gawedzki.backend.application.deal.dealsource;

import javax.persistence.Entity;

public enum DealSource {
    TAX,
    FOOD,
    INSTALMENT,
    INSURANCE,
    CLOTHING,
    GIFT,
    HOBBY,
    FUEL,
    FREETIME,
    ELECTRONICS,
    WORK,
    SCHOOL,
    UNIVERSITY,
    REPAIR,
    TRANSPORTATION,
    ENTERTAINMENT,
    SUBSCRIPTION,
    OTHER
}
