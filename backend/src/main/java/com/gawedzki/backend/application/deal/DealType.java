package com.gawedzki.backend.application.deal;

public enum DealType {
    EXPENSE,
    INCOME
}
