package com.gawedzki.backend.application.budget;

import com.gawedzki.backend.application.user.User;
import com.gawedzki.backend.application.user.UserRepository;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BudgetService {

    private final BudgetRepository budgetRepository;
    private final UserRepository userRepository;

    public BudgetService(BudgetRepository budgetRepository, UserRepository userRepository) {
        this.budgetRepository = budgetRepository;
        this.userRepository = userRepository;
    }

    public List<BudgetDto> list(BudgetQuery query) {

        Specification<Budget> specification = null;
        if (query.name != null && !query.name.isBlank()) {
            specification = BudgetSpecification.nameEquals(query.name).and(specification);
        }

        if (query.userId != null) {
            specification = BudgetSpecification.userIdEquals(query.userId).and(specification);
        }

        List<BudgetDto> budgetDtos = budgetRepository
                .findAll(specification)
                .stream()
                .map(BudgetDto::from)
                .collect(Collectors.toList());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
        System.out.println(LocalDateTime.now().format(formatter) + "  BudgetService: list()");

        return budgetDtos;
    }

    public BudgetDto create(BudgetRequest request) {

        User user = userRepository.getOne(request.id);

        Budget budget = new Budget();
        budget.setUser(user);
        budget.setDescription(request.description);
        budget.setName(request.name);
        budget.setFirstDay(request.firstDay);
//        Currency currency = Currency.getInstance(request.currencyCode.getCurrencyCode());
        budget.setCurrency(request.getCurrency());

//        System.out.println("getDisplayName(): " + currency.getDisplayName());
//        System.out.println("getCurrencyCode(): " + currency.getCurrencyCode());
//        System.out.println("getNumericCode(): " + currency.getNumericCode());
//        System.out.println("getSymbol(): " + currency.getSymbol());

        Budget save = budgetRepository.save(budget);
        return BudgetDto.from(save);
    }

    public BudgetDto findByName(Long id, String name) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
        System.out.println(LocalDateTime.now().format(formatter) + "  BudgetService: findByName()");
        name = name.toLowerCase();
        return BudgetDto.from(budgetRepository.findByUser_IdAndNameIgnoreCase(id, name));
    }

    public BudgetDto findById(Long id) {
        Budget byId = budgetRepository.findById(id).orElseThrow(() -> new RuntimeException("Budget not found"));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
        System.out.println(LocalDateTime.now().format(formatter) + "  BudgetService: findById()");
        return BudgetDto.from(byId);
    }

    public Double getLastNDaysBalance(Long id, Integer n) {
        Instant instant = Instant.now().minus(n, ChronoUnit.DAYS);
        return budgetRepository.getLastNDaysBalance(id, instant);
    }

    public void delete(Long id) {
        budgetRepository.deleteById(id);
    }
}
