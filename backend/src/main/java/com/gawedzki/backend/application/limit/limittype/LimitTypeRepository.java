package com.gawedzki.backend.application.limit.limittype;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LimitTypeRepository extends JpaRepository<LimitType, Long> {
}
