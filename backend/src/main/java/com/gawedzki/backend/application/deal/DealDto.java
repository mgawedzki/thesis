package com.gawedzki.backend.application.deal;

import com.gawedzki.backend.application.budget.BudgetDto;
import com.gawedzki.backend.application.deal.dealsource.DealSource;
import com.gawedzki.backend.application.file.FileDto;
import lombok.Data;

import java.time.Instant;

@Data
public class DealDto {

    public Long id;
    public DealType dealType;
    public DealSource dealSource;
    public String note;
    public Float value;
    public Instant datetime;
    public BudgetDto budget;
    public FileDto file;
    public Location location;

    public static DealDto from(Deal deal){
        FileDto fileDto = FileDto.from(deal);
        BudgetDto budgetDto = BudgetDto.excludeDeals(deal.getBudget());

        DealDto dto = new DealDto();
        dto.id = deal.getId();
        dto.dealType = deal.getDealType();
        dto.dealSource = deal.getDealSource();
        dto.note = deal.getNote();
        dto.value = deal.getValue();
        dto.datetime = deal.getDatetime();
        dto.budget = budgetDto;
        dto.location = deal.getLocation();
        dto.file = fileDto;

        return dto;
    }
}
