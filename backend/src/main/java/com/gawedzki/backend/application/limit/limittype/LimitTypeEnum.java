package com.gawedzki.backend.application.limit.limittype;

public enum LimitTypeEnum {
    SAVE_MONEY,
    SPEND_NOT_MORE_THAN
}
