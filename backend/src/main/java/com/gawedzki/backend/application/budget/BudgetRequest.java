package com.gawedzki.backend.application.budget;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.util.Currency;

@Getter
@Setter
public class BudgetRequest {

    @Getter(AccessLevel.NONE)
    public Long id;

    public String name;
    public String description;
    public int firstDay;
    public Currency currency;
}
