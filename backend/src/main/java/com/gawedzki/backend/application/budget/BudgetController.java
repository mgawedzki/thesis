package com.gawedzki.backend.application.budget;

import com.gawedzki.backend.application.security.AppUserDetails;
import com.gawedzki.backend.application.security.CurrentUser;
import org.springframework.web.bind.annotation.*;

import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
@RequestMapping("/budgets")
public class BudgetController {

    private final BudgetService budgetService;

    public BudgetController(BudgetService budgetService) {
        this.budgetService = budgetService;
    }

    @GetMapping
    public List<BudgetDto> list(@CurrentUser AppUserDetails userDetails, BudgetQuery query) {
        query.userId = userDetails.getId();
        List<BudgetDto> list = budgetService.list(query);
        return list;
    }

    @PostMapping
    public BudgetDto create(@CurrentUser AppUserDetails userDetails, @RequestBody BudgetRequest request) {
        request.id = userDetails.getId();
        return budgetService.create(request);
    }

//    @GetMapping("/{name}")
//    public BudgetDto getByName(@CurrentUser AppUserDetails userDetails, @PathVariable String name) {
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
//        System.out.println(LocalDateTime.now().format(formatter) + "  BudgetController: findByName()");
//        return budgetService.findByName(userDetails.getId(), name);
//    }

    @GetMapping("/{id}")
    public BudgetDto getById(@CurrentUser AppUserDetails userDetails, @PathVariable Long id) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
        System.out.println(LocalDateTime.now().format(formatter) + "  BudgetController: findById()");
        return budgetService.findById(id);
    }

    @GetMapping("/{id}/last_days/{n}")
    public Double getLastNDaysBalance(@CurrentUser AppUserDetails userDetails, @PathVariable Long id, @PathVariable Integer n) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
        System.out.println(LocalDateTime.now().format(formatter) + "  BudgetController: getLastNDaysBalance()");
        return budgetService.getLastNDaysBalance(id, n);
    }

    @DeleteMapping("/{id}")
    public void delete(@CurrentUser AppUserDetails userDetails, @PathVariable Long id){
        budgetService.delete(id);
    }

//    @GetMapping("/{id}")
//    public BudgetDto getById(@CurrentUser AppUserDetails userDetails, @PathVariable("id") Long id){
//        return budgetService.findById(id);
//    }
}
