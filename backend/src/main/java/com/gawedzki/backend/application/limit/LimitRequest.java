package com.gawedzki.backend.application.limit;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
public class LimitRequest {
    @Getter(AccessLevel.NONE)
    private Long id;
    private Float value;
    private Long limitTypeId;
    private Instant from;
    private Instant until;
    private Long budgetId;
}
