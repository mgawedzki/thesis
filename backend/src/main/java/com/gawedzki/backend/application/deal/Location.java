package com.gawedzki.backend.application.deal;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;

@Getter
@Setter
@Embeddable
public class Location {
    // szerokosc
    // 53.1324886
    private Double latitude;

    // dlugosc
    // 23.1688403
    private Double longitude;

    public Location(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Location() {
    }
}
