package com.gawedzki.backend.application.deal;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.gawedzki.backend.application.budget.Budget;
import com.gawedzki.backend.application.deal.dealsource.DealSource;
import com.gawedzki.backend.application.file.File;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;

@Getter
@Setter
@Entity
public class Deal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @GeneratedValue(generator = "UUID")
//    @GenericGenerator(
//            name = "UUID",
//            strategy = "org.hibernate.id.UUIDGenerator"
//    )
    private Long id;
    @Enumerated(EnumType.STRING)
    private DealType dealType;
    @Enumerated(EnumType.STRING)
    private DealSource dealSource;
    private String note;
    //    private String description;
    private Float value;
    private Instant datetime;
    //    @JsonManagedReference
    @ManyToOne()
    @JoinColumn(name = "budget_id")
    private Budget budget;
    @Embedded
    private Location location;

    @OneToOne(cascade = CascadeType.ALL)
    private File photo = new File();

    public Deal() {
    }

    public Deal(DealType dealType,
                DealSource dealSource,
                String note,
                Float value,
                Instant datetime,
                Location location,
                Budget budget,
                File photo) {
        this.dealType = dealType;
        this.dealSource = dealSource;
        this.note = note;
        this.value = value;
        this.datetime = datetime;
        this.location = location;
        this.budget = budget;
        this.photo = photo;
    }

    public float getSignedValue() {
        if (dealType.equals(DealType.INCOME)) return value;
        else return value *= -1;
    }

    public float getIfExpenseValue() {
        if (dealType.equals(DealType.EXPENSE)) return value;
        else return 0;
    }
}
