package com.gawedzki.backend.application.registration;

import com.gawedzki.backend.application.security.AppUserDetails;
import com.gawedzki.backend.application.security.AppUserDetailsService;
import com.gawedzki.backend.application.user.User;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RegistrationService {

    private final AppUserDetailsService appUserDetailsService;
    private final EmailValidator emailValidator;

    public String register(RegistrationRequest request) {
        boolean isValidEmail = emailValidator.test(request.getEmail());
        if (!isValidEmail) {
            throw new IllegalStateException("email not valid");
        }
        return appUserDetailsService.signUpUser(
                new User(
                        request.getUsername(),
                        request.getEmail(),
                        request.getPassword(),
                        true,
                        true,
                        true,
                        true
                )
        );
    }
}
