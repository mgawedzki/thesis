package com.gawedzki.backend.application.security;

import com.gawedzki.backend.application.budget.Budget;
import com.gawedzki.backend.application.budget.BudgetRepository;
import com.gawedzki.backend.application.budget.BudgetService;
import com.gawedzki.backend.application.user.User;
import com.gawedzki.backend.application.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class AppUserDetailsService implements UserDetailsService {

    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private BudgetRepository budgetRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void setBudgetRepository(BudgetRepository budgetRepository) {
        this.budgetRepository = budgetRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByUsername(username);
        user.orElseThrow(() -> new UsernameNotFoundException("Not found: " + username));
        AppUserDetails appUserDetails = user.map(AppUserDetails::new).get();
        return appUserDetails;
    }

    public String signUpUser(User user) {
        String message;

        boolean emailExists = userRepository.findByEmail(user.getEmail()).isPresent();
        boolean usernameExists = userRepository.findByUsername(user.getUsername()).isPresent();
        if (emailExists) {
            message = "email already taken";
            throw new IllegalStateException(message);
        }
        if (usernameExists) {
            message = "username already taken";
            throw new IllegalStateException(message);
        }
        String encodedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);
//        userRepository.save(user);

        Budget budget = new Budget();
        budget.setName("No. 1");
        budget.setFirstDay(1);
        budget.setUser(user);
        Set<Budget> budgets = new HashSet<>();
        budgets.add(budget);

        user.setBudgets(budgets);

        userRepository.save(user);
        budgetRepository.save(budget);

        // TODO: send confirmation token
        return "done";
    }
}
