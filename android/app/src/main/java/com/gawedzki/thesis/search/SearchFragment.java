package com.gawedzki.thesis.search;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.gawedzki.thesis.R;
import com.gawedzki.thesis.budget.Budget;
import com.gawedzki.thesis.budget.BudgetQuery;
import com.gawedzki.thesis.budget.BudgetRepository;
import com.gawedzki.thesis.deal.Deal;
import com.gawedzki.thesis.deal.DealRepository;
import com.gawedzki.thesis.menu.management.budgets.BudgetAdapter;
import com.gawedzki.thesis.menu.management.deals.DealAdapter;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class SearchFragment extends Fragment {

    private BudgetRepository budgetRepository;
    private DealRepository dealRepository;

    private SearchView searchView;
    private TextView dealInfo, budgetsTextView, dealsTextView;
    private RecyclerView dealRecycler, budgetRecycler;
    private BudgetAdapter budgetAdapter;
    private DealAdapter dealAdapter;
    private ToggleButton budgetsToggle, dealsToggle;
    private List<Budget> budgets = new LinkedList<>();
    private List<Deal> deals = new LinkedList<>();
    private View separateLineOfBudgets, separateLineOfDeals;

    public SearchFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();

        setBudgetsRecycler();
        setDealsRecycler();
        setToggleListeners();
        setSearchListener();

    }

    private void setToggleListeners() {
        budgetsToggle.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                budgetRecycler.setVisibility(View.VISIBLE);
                budgetsTextView.setVisibility(View.VISIBLE);
                separateLineOfBudgets.setVisibility(View.VISIBLE);
                budgetRecycler.setAdapter(budgetAdapter);
                budgetAdapter.getFilter().filter(searchView.getQuery().toString());
            } else {
                budgetRecycler.setVisibility(View.GONE);
                budgetsTextView.setVisibility(View.GONE);
                separateLineOfBudgets.setVisibility(View.GONE);
            }
        });

        dealsToggle.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                dealRecycler.setVisibility(View.VISIBLE);
                dealsTextView.setVisibility(View.VISIBLE);
                separateLineOfDeals.setVisibility(View.VISIBLE);
                dealRecycler.setAdapter(dealAdapter);
                dealAdapter.getFilter().filter(searchView.getQuery().toString());
            } else {
                dealRecycler.setVisibility(View.GONE);
                dealsTextView.setVisibility(View.GONE);
                separateLineOfDeals.setVisibility(View.GONE);
            }
        });
    }

    private void setSearchListener() {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (searchView.getQuery().length() == 0) newText = " ";
                if (dealsToggle.isChecked()) {
                    dealRecycler.setAdapter(dealAdapter);
                    dealAdapter.getFilter().filter(newText);
                }
                if (budgetsToggle.isChecked()) {
                    budgetRecycler.setAdapter(budgetAdapter);
                    budgetAdapter.getFilter().filter(newText);
                }
                return false;
            }
        });
    }

    private void setDealsRecycler() {
        dealRepository = new DealRepository(getActivity().getApplication());

        dealAdapter = new DealAdapter(getActivity().getApplicationContext(), deals);
        dealAdapter.submitList(deals);
        dealRecycler.setAdapter(dealAdapter);

        dealRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        dealRecycler.setHasFixedSize(true);

        dealRepository.getmAllDeals().observe(getViewLifecycleOwner(), deals1 -> {
            deals.addAll(deals1);
            dealAdapter = new DealAdapter(getActivity().getApplicationContext(), deals);
            dealAdapter.submitList(deals);
            dealRecycler.setAdapter(dealAdapter);
        });
        dealRepository.getAllDeals();

    }

    private void setBudgetsRecycler() {
        budgetRepository = new BudgetRepository(getActivity().getApplication());

        budgetAdapter = new BudgetAdapter(getActivity().getApplicationContext(), budgets);
        budgetAdapter.submitList(budgets);
        budgetRecycler.setAdapter(budgetAdapter);

        budgetRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        budgetRecycler.setHasFixedSize(true);

        budgetRepository.getmBudgets().observe(getViewLifecycleOwner(), budgets1 -> {
            budgets.addAll(budgets1);
            budgetAdapter = new BudgetAdapter(getActivity().getApplicationContext(), budgets);
            budgetAdapter.submitList(budgets);
            budgetRecycler.setAdapter(budgetAdapter);
        });
        budgetRepository.getAll(new BudgetQuery());

    }

    private void initViews() {
        this.searchView = getView().findViewById(R.id.search_bar);
        this.budgetRecycler = getView().findViewById(R.id.search_budget_recycler);
        this.dealRecycler = getView().findViewById(R.id.search_deal_recycler);
        this.budgetsToggle = getView().findViewById(R.id.search_budgets_toggle);
        this.dealsToggle = getView().findViewById(R.id.search_deals_toggle);
        this.budgetsTextView = getView().findViewById(R.id.search_budgets_text);
        this.dealsTextView = getView().findViewById(R.id.search_deals_text);
        this.separateLineOfBudgets = getView().findViewById(R.id.separate_line);
        this.separateLineOfDeals = getView().findViewById(R.id.separate_line_2);

        budgetRecycler.setNestedScrollingEnabled(false);
        dealRecycler.setNestedScrollingEnabled(false);

        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
    }
}