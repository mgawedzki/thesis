package com.gawedzki.thesis.menu.management.deals;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.gawedzki.thesis.R;
import com.gawedzki.thesis.deal.Deal;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class DealAdapter extends ListAdapter<Deal, DealAdapter.DealHolder> implements Filterable {

    private Context context;
    private List<Deal> deals;
    private List<Deal> filteredDeals;

    public DealAdapter(Context context, List<Deal> deals) {
        super(DIFF_CALLBACK);
        this.context = context;
        this.deals = deals;
        if (deals != null) {
            this.filteredDeals = new LinkedList<>(deals);
        }
    }

    private static final DiffUtil.ItemCallback<Deal> DIFF_CALLBACK = new DiffUtil.ItemCallback<Deal>() {
        @Override
        public boolean areItemsTheSame(@NonNull Deal oldItem, @NonNull Deal newItem) {
            return oldItem.getId().longValue() == newItem.getId().longValue();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Deal oldItem, @NonNull Deal newItem) {
            boolean b1 = oldItem.getNote().equals(newItem.getNote()) &&
                    oldItem.getValue().equals(newItem.getValue()) &&
                    oldItem.getDatetime().equals(newItem.getDatetime()) &&
                    oldItem.getDealSource().equals(newItem.getDealSource()) &&
                    oldItem.getDealType().equals(newItem.getDealType()) &&
                    oldItem.getBudget().getId().doubleValue() == newItem.getBudget().getId().doubleValue();

            boolean b2 = false;
            if (oldItem.getFile() != null && newItem.getFile() != null) {
                if (oldItem.getFile().getUrl().equals(newItem.getFile().getUrl())) {
                    b2 = true;
                }
            }

            boolean b3 = false;
            if (oldItem.getLocation() != null && newItem.getLocation() != null) {
                if (oldItem.getLocation().getLatitude().doubleValue() == newItem.getLocation().getLatitude().doubleValue() &&
                        oldItem.getLocation().getLongitude().doubleValue() == newItem.getLocation().getLongitude().doubleValue()) {
                    b3 = true;
                }
            }

            return b1 | b2 | b3;
        }
    };

    @NonNull
    @Override
    public DealHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.deal_item, parent, false);
        return new DealHolder(itemView);
    }

    @SuppressLint({"ResourceAsColor", "ResourceType", "DefaultLocale"})
    @Override
    public void onBindViewHolder(@NonNull DealHolder holder, int position) {
        Deal currentDeal = getItem(position);
        LocalDateTime localDateTime = LocalDateTime.ofInstant(currentDeal.getDatetime(), ZoneId.systemDefault());
        String dateStr = localDateTime.getDayOfMonth() + "-" +
                localDateTime.getMonthValue() + "-" +
                localDateTime.getYear() + " " +
                localDateTime.getHour() + ":" +
                localDateTime.getMinute();

        holder.date.setText(dateStr);

        String localeString = context.getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE).getString("default_locale", "en");
        Locale locale = new Locale(localeString);

        double signedValue = currentDeal.getValue();
        if (currentDeal.getDealType().equals("EXPENSE")) signedValue *= -1;
        holder.value.setText(
                String.format("%.2f (%s)", signedValue, currentDeal.getBudget().getCurrency().getSymbol(locale)));
        if (currentDeal.getNote() != null && !currentDeal.getNote().isEmpty()) {
            holder.note.setText(currentDeal.getNote());
        }
        holder.id.setText(currentDeal.getId() + ". ");
        holder.budgetName.setText(currentDeal.getBudget().getName());
        holder.line.getLayoutParams().width = holder.budgetName.getLayoutParams().width;
        if (currentDeal.getFile() != null) {
            holder.photoIcon.setVisibility(View.VISIBLE);
        }
        if (currentDeal.getLocation() != null && currentDeal.getLocation().getLatitude() != null) {
            holder.locationIcon.setVisibility(View.VISIBLE);
            if (holder.photoIcon.getVisibility() == View.GONE) {
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) holder.locationIcon.getLayoutParams();
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_END, RelativeLayout.TRUE);
            }
        }

        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = context.getTheme();
        if (currentDeal.getValue() > 0) {
//            holder.cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.green_positive));
        } else {
//            holder.cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.red_negative));
        }

        holder.cardView.setOnClickListener(v -> {
            Intent intent = new Intent(context, DealEditActivity.class);
            Bundle bundle = new Bundle();

            bundle.putLong("id", currentDeal.getId());
            bundle.putLong("budgetId", currentDeal.getBudget().getId());
            bundle.putString("note", currentDeal.getNote());
            bundle.putDouble("value", currentDeal.getValue());
            bundle.putString("filePath", currentDeal.getFilePath());
            bundle.putString("datetime", currentDeal.getDatetime().toString());
            bundle.putString("dealType", currentDeal.getDealType());
            bundle.putString("dealSource", currentDeal.getDealSource());
            bundle.putSerializable("deal", currentDeal);
            bundle.putSerializable("budget", currentDeal.getBudget());
            if (currentDeal.getLocation() != null) {
                bundle.putDouble("latitude", currentDeal.getLocation().getLatitude());
                bundle.putDouble("longitude", currentDeal.getLocation().getLongitude());
            }

            intent.putExtras(bundle);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        });
    }

    public Deal getDealAt(int position) {
        return getItem(position);
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Deal> filtered;

            if (constraint == null || constraint.length() == 0) {
                filtered = new LinkedList<>(deals);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                filtered = filteredDeals.stream().filter(deal -> deal.getNote().toLowerCase().trim().contains(filterPattern)).collect(Collectors.toList());
            }

            FilterResults results = new FilterResults();
            results.values = filtered;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            deals.clear();
            deals.addAll((List) results.values);

            notifyDataSetChanged();
        }
    };

    class DealHolder extends RecyclerView.ViewHolder {
        private TextView date;
        private TextView value;
        private TextView note;
        private TextView id;
        private TextView budgetName;
        private CardView cardView;
        private View line;
        private ImageView photoIcon, locationIcon;

        public DealHolder(@NonNull View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.deal_item_date);
            value = itemView.findViewById(R.id.deal_item_value);
            note = itemView.findViewById(R.id.deal_item_note);
            id = itemView.findViewById(R.id.deal_item_id);
            budgetName = itemView.findViewById(R.id.deal_item_budget_name);
            cardView = itemView.findViewById(R.id.deal_item_card_view);
            line = itemView.findViewById(R.id.separate_line);
            photoIcon = itemView.findViewById(R.id.deal_item_photo_icon);
            locationIcon = itemView.findViewById(R.id.deal_item_location_icon);
        }
    }
}
