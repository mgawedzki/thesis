package com.gawedzki.thesis.budget;

import lombok.Data;

@Data
public class BudgetQuery {
    public String name = "";
    public String description = "";
    public String firstDay = "";

    public BudgetQuery() {
    }

    public BudgetQuery(String name, String description, String firstDay) {
        this.name = name;
        this.description = description;
        this.firstDay = firstDay;
    }

    public BudgetQuery(String name) {
        this.name = name;
    }
}
