package com.gawedzki.thesis.budget;

import androidx.lifecycle.LiveData;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface BudgetRemoteDao {

    @GET("/budgets")
    Call<List<Budget>> getBudgets(@Header("Authorization") String token, @QueryMap Map<String, String> params);

    @POST("/budgets")
    Call<Budget> post(@Header("Authorization") String token);

//    @GET("/budgets/{budgetId}")
//    Call<LiveData<Budget>> getBudgetById(@Header("Authorization") String token, @Path("budgetId") Long budgetId);

    @GET("/budgets/{budgetName}")
    Call<Budget> getBudgetByName(@Header("Authorization") String token, @Path("budgetName") String budgetName);

    @GET("/budgets/{id}/last_days/{n}")
    Call<Double> getLastNdaysBalance(@Header("Authorization") String token, @Path("id") Long budgetId, @Path("n") int n);

    @DELETE("/budgets/{id}")
    Call<Void> delete(@Header("Authorization") String token, @Path("id") Long id);

    @POST("/budgets")
    Call<Budget> save(@Header("Authorization") String token, @Body Budget budget);
}
