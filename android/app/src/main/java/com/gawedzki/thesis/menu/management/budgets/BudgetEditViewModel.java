package com.gawedzki.thesis.menu.management.budgets;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.gawedzki.thesis.budget.Budget;
import com.gawedzki.thesis.budget.BudgetQuery;
import com.gawedzki.thesis.budget.BudgetRepository;

import java.util.List;

public class BudgetEditViewModel extends AndroidViewModel {

    private BudgetRepository budgetRepository;
    private MutableLiveData<List<Budget>> allBudgets;

    public BudgetEditViewModel(@NonNull Application application) {
        super(application);
        budgetRepository = new BudgetRepository(application);
        allBudgets = getAllBudgets();
        budgetRepository.getAll(new BudgetQuery());
    }

    public MutableLiveData<List<Budget>> getAllBudgets() {
        return budgetRepository.getmBudgets();
    }

    public void updateBudgets(){
        budgetRepository.getAll(new BudgetQuery());
    }

    public void delete(Budget budget){
        budgetRepository.delete(budget);
        List<Budget> value = allBudgets.getValue();
        value.remove(budget);
        allBudgets.setValue(value);
    }
}
