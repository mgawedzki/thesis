package com.gawedzki.thesis.menu;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gawedzki.thesis.menu.management.ManagementActivity;
import com.gawedzki.thesis.R;
import com.gawedzki.thesis.menu.limits.LimitsActivity;
import com.gawedzki.thesis.menu.reports.ReportActivity;

public class MenuFragment extends Fragment {

    CardView manageButton;
    CardView goalsLimits;
    CardView reportButton;
    CardView exportButton;
    TextView topText;

    MenuViewModel viewModel;

    public MenuFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(MenuViewModel.class);
    }



    public void initViews() {
        manageButton = getView().findViewById(R.id.menu_management_cardview);
        goalsLimits = getView().findViewById(R.id.menu_goals_limits_cardview);
        reportButton = getView().findViewById(R.id.menu_report_cardview);
//        exportButton = getView().findViewById(R.id.export_cardview);
        topText = getView().findViewById(R.id.topText);
//
        manageButton.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), ManagementActivity.class);
            startActivity(intent);
        });

        goalsLimits.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), LimitsActivity.class);
            startActivity(intent);
        });

        reportButton.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), ReportActivity.class);
            startActivity(intent);
        });
//
//        exportButton.setOnClickListener(v -> {
////            Intent intent = new Intent(getContext(), GoalsLimitsActivity.class);
////            startActivity(intent);
//            Uri gmmIntentUri = Uri.parse("geo:0,0?q=53.1324886,23.1688403");
//            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//            if (mapIntent.setPackage("com.google.android.apps.maps") != null) {
//                startActivity(mapIntent);
//            }
//        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_menu, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();

        viewModel.getMe()
                .observe(getViewLifecycleOwner(), user -> {
                    topText.setText(getResources().getString(R.string.hi) + ", " + user.getUsername());
                });

    }
}