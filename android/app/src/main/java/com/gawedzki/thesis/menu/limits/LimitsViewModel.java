package com.gawedzki.thesis.menu.limits;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.gawedzki.thesis.budget.Budget;
import com.gawedzki.thesis.budget.BudgetQuery;
import com.gawedzki.thesis.budget.BudgetRepository;
import com.gawedzki.thesis.limit.Limit;
import com.gawedzki.thesis.limit.LimitRepository;
import com.gawedzki.thesis.limit.LimitRequest;
import com.gawedzki.thesis.limit.LimitType;
import com.gawedzki.thesis.limit.LimitTypeEnum;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;

public class LimitsViewModel extends AndroidViewModel {

    private BudgetRepository budgetRepository;
    private LimitRepository limitRepository;

    private MutableLiveData<List<Limit>> mLimits;
    private MutableLiveData<List<Budget>> mBudgets;
    private MutableLiveData<Budget> selectedBudget;

    public LimitsViewModel(@NonNull Application application) {
        super(application);

        limitRepository = new LimitRepository(application);
        budgetRepository = new BudgetRepository(application);

        mLimits = limitRepository.getMlimits();
        mBudgets = budgetRepository.getmBudgets();
        selectedBudget = new MutableLiveData<>();
    }

    public MutableLiveData<List<Limit>> getmLimits() {
        return mLimits;
    }

    public void setmLimits(List<Limit> limits) {
        this.mLimits.setValue(limits);
    }

    public MutableLiveData<List<Budget>> getmBudgets() {
        return mBudgets;
    }

    public MutableLiveData<Budget> getSelectedBudget() {
        return selectedBudget;
    }

    public void setSelectedBudget(Budget selectedBudget) {
        this.selectedBudget.setValue(selectedBudget);
    }

    public void save(Limit limit) {
        LimitRequest request = new LimitRequest();
        request.setBudgetId(limit.getBudget().getId());
        request.setFrom(limit.getFrom().toString());
        request.setUntil(limit.getUntil().toString());
        request.setValue(limit.getGoalValue());
        request.setLimitTypeId(limit.getLimitType().getId());
        limitRepository.save(request);
    }

    public void updateLimits() {
//        populateLimitsLocally();
        fetchDbData();
    }

    private void fetchDbData() {
        limitRepository.getAll();
    }

    private void populateLimitsLocally() {
        budgetRepository.getAll(new BudgetQuery());
        List<Limit> limits = new LinkedList<>();

        Limit limit = new Limit(
                (long) 1,
                (float) 300,
                new LimitType((long) 1, LimitTypeEnum.SAVE_MONEY),
                Instant.now().minus(30, ChronoUnit.DAYS),
                Instant.now().plus(14, ChronoUnit.DAYS),
                new Budget(),
                50
        );
        limits.add(limit);

        limit = new Limit(
                (long) 2,
                (float) 200,
                new LimitType((long) 1, LimitTypeEnum.SPEND_NOT_MORE_THAN),
                Instant.now().minus(2, ChronoUnit.DAYS),
                Instant.now().plus(7, ChronoUnit.DAYS),
                new Budget(),
                30
        );
        limits.add(limit);

        mLimits.setValue(limits);
    }

    public void updateBudgets() {
        budgetRepository.getAll(new BudgetQuery());
    }

    public void delete(Limit limit) {
        limitRepository.delete(limit.getId());
//        List<Limit> value = mLimits.getValue();
//        value.remove(limit);
//        mLimits.setValue(value);
    }
}
