package com.gawedzki.thesis.signin.register;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.gawedzki.thesis.R;
import com.gawedzki.thesis.api.RetrofitInstance;
import com.gawedzki.thesis.signin.login.LoginService;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends AppCompatActivity {

    private EditText usernameEditText, emailEditText, passwordEditText;
    private Button registerBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        initView();
        setListeners();
    }

    private void setListeners() {
        registerBtn.setOnClickListener(v -> {
            LoginService loginService = RetrofitInstance.getRetrofit().create(LoginService.class);
            RegistrationRequest request = new RegistrationRequest();
            request.setUsername(usernameEditText.getText().toString());
//            request.setEmail(emailEditText.getText().toString());
            request.setPassword(passwordEditText.getText().toString());

            Call<ResponseBody> registerCall = loginService.register(request);

            registerCall.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(RegistrationActivity.this, response.body().toString(), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Toast.makeText(RegistrationActivity.this, R.string.ops_sth_went_wrong, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(RegistrationActivity.this, R.string.couldnt_connect_with_server, Toast.LENGTH_SHORT).show();
                }
            });
        });
    }

    private void initView() {
        usernameEditText = findViewById(R.id.register_username);
//        emailEditText = findViewById(R.id.register_email);
        passwordEditText = findViewById(R.id.register_password);
        registerBtn = findViewById(R.id.register_btn);
    }
}