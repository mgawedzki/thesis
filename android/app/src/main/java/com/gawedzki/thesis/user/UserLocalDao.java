package com.gawedzki.thesis.user;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface UserLocalDao {

    @Insert
    void create(User user);

    @Update
    void update(User user);

    @Delete
    void delete(User user);

    @Query("DELETE FROM user_acc")
    void deleteAllUsers();

    @Query("SELECT * FROM user_acc")
    LiveData<List<User>> getAllUsers();

    @Query("SELECT * FROM user_acc")
    LiveData<User> getMe();
}
