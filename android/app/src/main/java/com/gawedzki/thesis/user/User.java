package com.gawedzki.thesis.user;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import com.gawedzki.thesis.converters.Converter;
import com.gawedzki.thesis.deal.Deal;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.UUID;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(tableName = "user_acc")
public class User {

    @PrimaryKey(autoGenerate = true)
    private Long id;

    @SerializedName("username")
    private String username;

    @SerializedName("email")
    private String email;

    @TypeConverters(Converter.class)
    @SerializedName("deals")
    private List<Deal> deals;

    public User() {
    }

    public User(String username, String email) {
        this.username = username;
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Deal> getDeals() {
        return deals;
    }

    public void setDeals(List<Deal> deals) {
        this.deals = deals;
    }
}
