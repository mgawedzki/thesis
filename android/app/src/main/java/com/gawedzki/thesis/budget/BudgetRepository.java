package com.gawedzki.thesis.budget;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.gawedzki.thesis.R;
import com.gawedzki.thesis.api.RetrofitInstance;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class BudgetRepository {

    private BudgetRemoteDao budgetRemoteDao;
    private Context context;
    private String token;
    List<Budget> budgets;
    MutableLiveData<List<Budget>> mBudgets = new MutableLiveData<>();
    MutableLiveData<Double> mLastNdaysBalance = new MutableLiveData<>();

    public BudgetRepository(Application application) {
        context = application.getApplicationContext();
        Retrofit retrofit = RetrofitInstance.getRetrofit();
        budgetRemoteDao = retrofit.create(BudgetRemoteDao.class);
        SharedPreferences sharedPreferences = context.getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token", "00000");
    }

    public LiveData<Budget> getByName(String name) {

        MutableLiveData<Budget> mBudget = new MutableLiveData<>();
        Call<Budget> call = budgetRemoteDao.getBudgetByName(token, name);
        call.enqueue(new Callback<Budget>() {
            @Override
            public void onResponse(Call<Budget> call, Response<Budget> response) {
                mBudget.setValue(response.body());
            }

            @Override
            public void onFailure(Call<Budget> call, Throwable t) {

            }
        });
        return mBudget;
    }

    public void getAll(BudgetQuery query) {

        Map<String, String> params = new HashMap<>();
        params.put("name", query.name);
        params.put("description", query.description);
        params.put("firstDay", query.firstDay);

        Call<List<Budget>> call = budgetRemoteDao.getBudgets(token, params);
        call.enqueue(new Callback<List<Budget>>() {
            @Override
            public void onResponse(Call<List<Budget>> call, Response<List<Budget>> response) {
                if (response.isSuccessful()) {
                    List<Budget> body = response.body();
                    mBudgets.setValue(body);
                } else {
                    Toast.makeText(context, R.string.ops_sth_went_wrong, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Budget>> call, Throwable t) {
                Log.v("tag", t.getMessage());

                Toast.makeText(context, R.string.couldnt_connect_with_server, Toast.LENGTH_SHORT).show();

            }
        });
//        Log.v("tag", String.valueOf(mBudgets.getValue().size()));
    }

    public MutableLiveData<List<Budget>> getmBudgets() {
        return mBudgets;
    }

    public void getAll() {

        BudgetQuery query = new BudgetQuery();
        Map<String, String> params = new HashMap<>();
        params.put("name", query.name);
        params.put("description", query.description);
        params.put("firstDay", query.firstDay);


        Call<List<Budget>> call = budgetRemoteDao.getBudgets(token, params);
        call.enqueue(new Callback<List<Budget>>() {
            @Override
            public void onResponse(Call<List<Budget>> call, Response<List<Budget>> response) {
                if (response.code() == 200) {
                    List<Budget> body = response.body();
                    mBudgets.setValue(body);
                    Log.v("tag", response.message());

                } else {
                    Log.v("tag", "Error" + response.errorBody().toString());
                    Toast.makeText(context, R.string.ops_sth_went_wrong, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Budget>> call, Throwable t) {
                Log.v("tag", t.getMessage());
                Toast.makeText(context, R.string.couldnt_connect_with_server, Toast.LENGTH_SHORT).show();

            }
        });
        Log.v("tag", String.valueOf(mBudgets.getValue().size()));
    }

    public LiveData<Double> getlastNdaysBalance(Long budgetId, int n) {

        Call<Double> call = budgetRemoteDao.getLastNdaysBalance(token, budgetId, n);
        call.enqueue(new Callback<Double>() {
            @Override
            public void onResponse(Call<Double> call, Response<Double> response) {
                if (response.isSuccessful()) {
                    Double body = response.body();
                    mLastNdaysBalance.setValue(body);
                    Log.v("tag", response.message());

                } else {
                    Toast.makeText(context, R.string.ops_sth_went_wrong, Toast.LENGTH_SHORT).show();
                    Log.v("tag", String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<Double> call, Throwable t) {
                Toast.makeText(context, R.string.couldnt_connect_with_server, Toast.LENGTH_SHORT).show();
                Log.v("tag", String.valueOf(t.getMessage()));
            }
        });
        return mLastNdaysBalance;
    }

    public MutableLiveData<Double> getmLastNdaysBalance() {
        return mLastNdaysBalance;
    }

    public void delete(Budget budget) {
        Call<Void> call = budgetRemoteDao.delete(token, budget.getId());
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(context, R.string.completed_succesfully, Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(context, R.string.ops_sth_went_wrong, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(context, R.string.couldnt_connect_with_server, Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void save(Budget budget) {
        Call<Budget> call = budgetRemoteDao.save(token, budget);

        call.enqueue(new Callback<Budget>() {
            @Override
            public void onResponse(Call<Budget> call, Response<Budget> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(context, R.string.completed_succesfully, Toast.LENGTH_SHORT).show();
//                    List<Budget> value = mBudgets.getValue();
//                    value.add(response.body());
//                    mBudgets.setValue(value);
                    return;
                }
                Toast.makeText(context, R.string.ops_sth_went_wrong, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Budget> call, Throwable t) {
                Toast.makeText(context, R.string.couldnt_connect_with_server, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
