package com.gawedzki.thesis.menu.management.deals;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.gawedzki.thesis.R;

public class DealsManagement extends AppCompatActivity {

    private DealEditViewModel dealEditViewModel;
    private ImageButton refresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deals_management);

        refresh = findViewById(R.id.deal_list_refresh);

//        dealViewModel = new ViewModelProvider(this, getDefaultViewModelProviderFactory()).get(DealViewModel.class);
        dealEditViewModel = ViewModelProviders.of(this).get(DealEditViewModel.class);
//        dealViewModel = new DealViewModel(getApplication());
//        dealViewModel = new ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory.getInstance(getApplication())).get(DealViewModel.class);

        RecyclerView recyclerView = findViewById(R.id.deals_recycler);
        TextView info = findViewById(R.id.deal_list_info);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        DealAdapter adapter = new DealAdapter(getApplicationContext(), dealEditViewModel.getAllDeals().getValue());
        recyclerView.setAdapter(adapter);

        dealEditViewModel.getAllDeals().observe(this, deals -> {
            if (deals.size() == 0) {
                info.setText(R.string.empty);
                info.setVisibility(View.VISIBLE);
            } else {
                adapter.submitList(deals);
                info.setVisibility(View.GONE);
            }
        });

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                showDeleteAlertDialog(viewHolder, direction, adapter);
            }
        }).attachToRecyclerView(recyclerView);

        refresh.setOnClickListener(v -> {
            dealEditViewModel.updateDeals();
            Toast.makeText(this, R.string.updating, Toast.LENGTH_SHORT).show();
        });

    }

    private void showDeleteAlertDialog(@NonNull RecyclerView.ViewHolder viewHolder, int direction, DealAdapter adapter) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_q)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, (dialog, id) -> {
                    dealEditViewModel.delete(adapter.getDealAt(viewHolder.getAdapterPosition()));
                    adapter.notifyItemRemoved(viewHolder.getAdapterPosition());
                })
                .setNegativeButton(R.string.no, (dialog, id) -> {
                    dialog.cancel();
                    adapter.notifyItemChanged(viewHolder.getAdapterPosition());
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}