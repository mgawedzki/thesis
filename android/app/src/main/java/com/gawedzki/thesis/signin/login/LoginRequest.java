package com.gawedzki.thesis.signin.login;

import lombok.Data;

@Data
public class LoginRequest {
    String username;
    String password;

    LoginRequest(String username, String password){
        this.username = username;
        this.password = password;
    }
}
