package com.gawedzki.thesis.settings;

import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.gawedzki.thesis.AboutAppActivity;
import com.gawedzki.thesis.R;
import com.gawedzki.thesis.signin.login.LoginActivity;
import com.google.android.material.switchmaterial.SwitchMaterial;

import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class SettingsFragment extends Fragment {

    private SwitchMaterial themeSwitch, locationSwitch;
    private RelativeLayout layout;
    private Spinner dealsourceSpinner, dealtypeSpinner, languageSpinner;
    private EditText lastNdays;
    private Button logout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLocale();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        setThemeSwitch();
        setListeners();
    }

    private void setThemeSwitch() {
        SharedPreferences sharedPrefs = this.getActivity().getSharedPreferences("sharedPrefs", MODE_PRIVATE);
        int currentTheme = sharedPrefs.getInt("mytheme", AppCompatDelegate.MODE_NIGHT_YES);

        if (currentTheme == AppCompatDelegate.MODE_NIGHT_NO) {
            themeSwitch.setChecked(false);
        } else if (currentTheme == AppCompatDelegate.MODE_NIGHT_YES) {
            themeSwitch.setChecked(true);
        }
    }

    private void setListeners() {
        SharedPreferences sharedPrefsEditor = this.getActivity().getSharedPreferences("sharedPrefs", MODE_PRIVATE);
        themeSwitch.setOnClickListener(v -> {
            SharedPreferences.Editor editor = sharedPrefsEditor.edit();

            if (themeSwitch.isChecked()) {
                layout.setBackgroundColor(Color.rgb(191, 191, 191));
                editor.putInt("mytheme", AppCompatDelegate.MODE_NIGHT_YES);
            } else {
                layout.setBackgroundColor(Color.rgb(84, 84, 84));
                editor.putInt("mytheme", AppCompatDelegate.MODE_NIGHT_NO);
            }
            editor.apply();

        });

        locationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = sharedPrefsEditor.edit();

                if (isChecked) editor.putBoolean("default_location", true);
                else editor.putBoolean("default_location", false);

                editor.apply();
            }
        });

        dealtypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SharedPreferences.Editor editor = sharedPrefsEditor.edit();
                String dealtype = dealtypeSpinner.getSelectedItem().toString();
                editor.putString("default_dealtype", dealtype);
                editor.apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        dealsourceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SharedPreferences.Editor editor = sharedPrefsEditor.edit();
                int selectedItemPosition = dealsourceSpinner.getSelectedItemPosition();
                editor.putInt("default_dealsource", selectedItemPosition);
                editor.apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        lastNdays.addTextChangedListener(new TextWatcher() {
            final SharedPreferences.Editor editor = sharedPrefsEditor.edit();

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    editor.putInt("default_last_n_days", Integer.parseInt(s.toString()));
                    editor.apply();
                }
                ;
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        languageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String locale = "";
                if (position == 0) locale = "en";
                else if (position == 1) locale = "pl";
                setlocale(locale);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        logout.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(R.string.delete_q)
                    .setCancelable(false)
                    .setPositiveButton(R.string.yes, (dialog, id) -> {
                        SharedPreferences.Editor editor = getActivity().getSharedPreferences("sharedPrefs", MODE_PRIVATE).edit();
                        editor.remove("token");
                        editor.apply();
                        Intent logoutIntent = new Intent(getActivity(), LoginActivity.class);
                        startActivity(logoutIntent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
                        getActivity().finish();
                    })
                    .setNegativeButton(R.string.no, (dialog, id) -> {
                        dialog.cancel();
                    });
            AlertDialog alert = builder.create();
            alert.show();
        });

        getView().findViewById(R.id.settings_about_app).setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), AboutAppActivity.class);
            startActivity(intent);
        });
    }

    private void initViews() {
        themeSwitch = getView().findViewById(R.id.settings_theme_switch);
        layout = getView().findViewById(R.id.settings_fragment);
        locationSwitch = getView().findViewById(R.id.settings_location_switch);
        dealsourceSpinner = getView().findViewById(R.id.settings_dealsource_spinner);
        dealtypeSpinner = getView().findViewById(R.id.settings_dealtype_spinner);
        lastNdays = getView().findViewById(R.id.settings_last_n_days);
        languageSpinner = getView().findViewById(R.id.settings_language_spinner);
        logout = getView().findViewById(R.id.logout_btn);

        SharedPreferences sharedPrefs = this.getActivity().getSharedPreferences("sharedPrefs", MODE_PRIVATE);
        boolean defaultLocation = sharedPrefs.getBoolean("default_location", false);
        String defaultDealtype = sharedPrefs.getString("default_dealtype", getResources().getString(R.string.income));
        int defaultDealsource = sharedPrefs.getInt("default_dealsource", 0);
        int defaultLastNdays = sharedPrefs.getInt("default_last_n_days", 7);
        String defaultLocale = sharedPrefs.getString("default_locale", "en");

        locationSwitch.setChecked(defaultLocation);
        dealsourceSpinner.setSelection(defaultDealsource, true);
        lastNdays.setText(String.valueOf(defaultLastNdays));
        if (defaultDealtype.equals(getResources().getString(R.string.income)))
            dealtypeSpinner.setSelection(0);
        else dealtypeSpinner.setSelection(1);
        if (defaultLocale.equals("en")) {
            languageSpinner.setSelection(0, true);
        } else {
            languageSpinner.setSelection(1, true);
        }
        setlocale(defaultLocale);
    }

    private void setlocale(String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getActivity().getBaseContext().getResources().updateConfiguration(config, getActivity().getBaseContext().getResources().getDisplayMetrics());

        SharedPreferences.Editor editor = getActivity().getSharedPreferences("sharedPrefs", MODE_PRIVATE).edit();
        editor.putString("default_locale", lang);
        editor.apply();
    }

    public void loadLocale() {
        SharedPreferences sharedPrefs = getActivity().getSharedPreferences("sharedPrefs", MODE_PRIVATE);
        String s = sharedPrefs.getString("default_locale", "en");
        setlocale(s);
    }
}