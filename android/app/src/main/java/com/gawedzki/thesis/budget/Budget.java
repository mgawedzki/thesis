package com.gawedzki.thesis.budget;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.gawedzki.thesis.converters.Converter;
import com.gawedzki.thesis.deal.Deal;

import java.io.Serializable;
import java.util.Currency;
import java.util.LinkedList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
//@Entity
public class Budget implements Serializable {

//    @PrimaryKey(autoGenerate = true)
    private Long id;
    private String name;
    private String description;
    private int firstDay;
    private Currency currency;
    @TypeConverters(Converter.class)
    private List<Deal> deals = new LinkedList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getFirstDay() {
        return firstDay;
    }

    public void setFirstDay(int firstDay) {
        this.firstDay = firstDay;
    }

    public List<Deal> getDeals() {
        return deals;
    }

    public void setDeals(List<Deal> deals) {
        this.deals = deals;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
