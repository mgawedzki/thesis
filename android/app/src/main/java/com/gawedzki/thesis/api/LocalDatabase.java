package com.gawedzki.thesis.api;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.gawedzki.thesis.deal.Deal;
//import com.gawedzki.thesis.deal.DealDao;
import com.gawedzki.thesis.user.User;
import com.gawedzki.thesis.user.UserLocalDao;

//@Database(entities = {User.class}, version = 1, exportSchema = false)
public abstract class LocalDatabase extends RoomDatabase {

    private static LocalDatabase instance;

    public abstract UserLocalDao userDao();
//    public abstract DealDao dealDao();

    public static synchronized LocalDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(), LocalDatabase.class, "local_database")
                    .fallbackToDestructiveMigration().build();
        }
        return instance;
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateUserAsyncTask(instance).execute();
        }
    };

    private static class PopulateUserAsyncTask extends AsyncTask<Void, Void, Void>{
        private UserLocalDao userLocalDao;

        private PopulateUserAsyncTask(LocalDatabase db){
            userLocalDao = db.userDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            userLocalDao.create(new User("michalll", "michalll@mail.com"));
            return null;
        }
    }
}
