package com.gawedzki.thesis.menu.reports;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import com.gawedzki.thesis.budget.Budget;
import com.gawedzki.thesis.budget.BudgetQuery;
import com.gawedzki.thesis.budget.BudgetRepository;
import com.gawedzki.thesis.deal.Deal;
import com.gawedzki.thesis.deal.DealRepository;

import java.util.List;

public class ReportViewModel extends AndroidViewModel {

    private BudgetRepository budgetRepository;
    private DealRepository dealRepository;

    private MutableLiveData<List<Budget>> mBudgets;
    private MutableLiveData<List<Deal>> mDeals;
    private MutableLiveData<Budget> mSelectedBudget;

    public ReportViewModel(@NonNull Application application) {
        super(application);
        budgetRepository = new BudgetRepository(application);
        dealRepository = new DealRepository(application);

        mBudgets = new MutableLiveData<>();
        mDeals = new MutableLiveData<>();
        mSelectedBudget = new MutableLiveData<>();

        mBudgets = budgetRepository.getmBudgets();
        mDeals = dealRepository.getmAllDeals();
    }

    public void updateBudgets(){
        mBudgets = budgetRepository.getmBudgets();
        budgetRepository.getAll(new BudgetQuery());
    }

    public void updateDeals(){
        mDeals = dealRepository.getmAllDeals();
        dealRepository.getAllDeals();
    }

    public LiveData<List<Budget>> getmBudgets() {
        return mBudgets;
    }

    public void setmBudgets(List<Budget> budgets) {
        this.mBudgets.setValue(budgets);
    }

    public LiveData<List<Deal>> getmDeals() {
        return mDeals;
    }

    public void setmDeals(List<Deal> deals) {
        this.mDeals.setValue(deals);
    }

    public MutableLiveData<Budget> getmSelectedBudget() {
        return mSelectedBudget;
    }

    public void setmSelectedBudget(Budget selectedBudget) {
        this.mSelectedBudget.setValue(selectedBudget);
    }
}
