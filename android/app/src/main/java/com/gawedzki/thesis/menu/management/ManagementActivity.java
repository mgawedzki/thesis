package com.gawedzki.thesis.menu.management;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;

import com.gawedzki.thesis.R;
import com.gawedzki.thesis.menu.management.budgets.BudgetsManagement;
import com.gawedzki.thesis.menu.management.deals.DealsManagement;

public class ManagementActivity extends AppCompatActivity {

    private CardView dealsButton;
    private CardView budgetsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_management);
        initViews();
        setListeners();
    }

    private void initViews() {
        dealsButton = findViewById(R.id.manage_deals);
        budgetsButton = findViewById(R.id.manage_budgets);
    }

    private void setListeners() {
        dealsButton.setOnClickListener(v -> {
            Intent intent = new Intent(this, DealsManagement.class);
            startActivity(intent);
        });
        budgetsButton.setOnClickListener(v -> {
            Intent intent = new Intent(this, BudgetsManagement.class);
            startActivity(intent);
        });
    }
}