package com.gawedzki.thesis.limit;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LimitType {
    private Long id;
    private LimitTypeEnum type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LimitTypeEnum getType() {
        return type;
    }

    public void setType(LimitTypeEnum type) {
        this.type = type;
    }

    public LimitType(Long id, LimitTypeEnum type) {
        this.id = id;
        this.type = type;
    }
}
