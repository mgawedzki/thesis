package com.gawedzki.thesis.limit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface LimitRemoteDao {

    String AUTHORIZATION_KEY = "Authorization";

    @GET("/limits/")
    Call<List<Limit>> getAll(@Header(AUTHORIZATION_KEY) String token);

    @POST("/limits/")
    Call<Limit> save(@Header(AUTHORIZATION_KEY) String token, @Body LimitRequest request);

    @DELETE("/limits/{id}")
    Call<Void> delete(@Header(AUTHORIZATION_KEY) String token, @Path("id") Long id);
}
