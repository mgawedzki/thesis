package com.gawedzki.thesis.limit;

import com.gawedzki.thesis.budget.Budget;

import java.time.Instant;

public class Limit {
    private Long id;
    private Float goalValue;
    private LimitType limitType;
    private Instant from;
    private Instant until;
    private Budget budget;
    private float balance;

    public Limit() {
    }

    public Limit(Long id, Float goalValue, LimitType limitType, Instant from, Instant until, Budget budget, float balance) {
        this.id = id;
        this.goalValue = goalValue;
        this.limitType = limitType;
        this.from = from;
        this.until = until;
        this.budget = budget;
        this.balance = balance;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getGoalValue() {
        return goalValue;
    }

    public void setGoalValue(Float goalValue) {
        this.goalValue = goalValue;
    }

    public LimitType getLimitType() {
        return limitType;
    }

    public void setLimitType(LimitType limitType) {
        this.limitType = limitType;
    }

    public Instant getFrom() {
        return from;
    }

    public void setFrom(Instant from) {
        this.from = from;
    }

    public Instant getUntil() {
        return until;
    }

    public void setUntil(Instant until) {
        this.until = until;
    }

    public Budget getBudget() {
        return budget;
    }

    public void setBudget(Budget budget) {
        this.budget = budget;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }
}
