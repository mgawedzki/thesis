package com.gawedzki.thesis.limit;

import com.gawedzki.thesis.budget.Budget;

import java.time.Instant;

public class LimitRequest {
    private Long id;
    private Float value;
    private Long limitTypeId;
    private String from;
    private String until;
    private Long budgetId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public Long getLimitTypeId() {
        return limitTypeId;
    }

    public void setLimitTypeId(Long limitTypeId) {
        this.limitTypeId = limitTypeId;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setUntil(String until) {
        this.until = until;
    }

    public Long getBudgetId() {
        return budgetId;
    }

    public void setBudgetId(Long budgetId) {
        this.budgetId = budgetId;
    }
}
