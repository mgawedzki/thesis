package com.gawedzki.thesis.limit;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;

import com.gawedzki.thesis.R;
import com.gawedzki.thesis.api.RetrofitInstance;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LimitRepository {

    private final String SHARED_PREFS = "sharedPrefs";
    private final String TOKEN_KEY = "token";
    private final String DEFAULT_TOKEN_VALUE = "token";

    private LimitRemoteDao limitRemoteDao;
    private Context context;
    private String token;

    private MutableLiveData<List<Limit>> mlimits;

    public LimitRepository(Application application){
        context = application.getApplicationContext();
        Retrofit retrofit = RetrofitInstance.getRetrofit();
        limitRemoteDao = retrofit.create(LimitRemoteDao.class);
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        token = sharedPreferences.getString(TOKEN_KEY, DEFAULT_TOKEN_VALUE);

        mlimits = new MutableLiveData<>();
    }

    public MutableLiveData<List<Limit>> getMlimits() {
        return mlimits;
    }

    public void setMlimits(List<Limit> limits) {
        this.mlimits.setValue(limits);
    }

    public void getAll(){
        Call<List<Limit>> call = limitRemoteDao.getAll(token);
        call.enqueue(new Callback<List<Limit>>() {
            @Override
            public void onResponse(Call<List<Limit>> call, Response<List<Limit>> response) {
                if (response.isSuccessful()) {
                    mlimits.setValue(response.body());
                    return;
                }
                Toast.makeText(context, R.string.ops_sth_went_wrong, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<List<Limit>> call, Throwable t) {
                Toast.makeText(context, R.string.couldnt_connect_with_server, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void save(LimitRequest request){
        Call<Limit> call = limitRemoteDao.save(token, request);
        call.enqueue(new Callback<Limit>() {
            @Override
            public void onResponse(Call<Limit> call, Response<Limit> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(context, R.string.completed_succesfully, Toast.LENGTH_SHORT).show();
                    List<Limit> value = mlimits.getValue();
                    value.add(response.body());
                    mlimits.setValue(value);
                    return;
                }
                Toast.makeText(context, R.string.ops_sth_went_wrong, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Limit> call, Throwable t) {
                Toast.makeText(context, R.string.couldnt_connect_with_server, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void delete(Long id) {
        Call<Void> call = limitRemoteDao.delete(token, id);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(context, R.string.completed_succesfully, Toast.LENGTH_SHORT).show();
                    List<Limit> limits = mlimits.getValue();
                    Limit deletedLimit = limits.stream().filter(limit -> limit.getId().equals(id)).findFirst().get();
                    limits.remove(deletedLimit);
                    mlimits.setValue(limits);
                    return;
                }
                Toast.makeText(context, R.string.ops_sth_went_wrong, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(context, R.string.couldnt_connect_with_server, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
