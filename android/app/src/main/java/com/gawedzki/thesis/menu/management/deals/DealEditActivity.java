package com.gawedzki.thesis.menu.management.deals;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gawedzki.thesis.R;
import com.gawedzki.thesis.budget.Budget;
import com.gawedzki.thesis.budget.BudgetQuery;
import com.gawedzki.thesis.budget.BudgetRepository;
import com.gawedzki.thesis.deal.Deal;
import com.gawedzki.thesis.deal.DealRepository;
import com.gawedzki.thesis.deal.DealRequest;
import com.gawedzki.thesis.image.ImageViewerActivity;
import com.google.android.material.snackbar.Snackbar;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class DealEditActivity extends AppCompatActivity {

    private static final String TAG = "tag";
    private Bundle extras;
    private BudgetRepository budgetRepository;
    private DealRepository dealRepository;
    private TextView date;
    private EditText note, value;
    private Spinner budgetSpinner, dealTypeSpinner, dealSourceSpinner;
    private Button changePhoto;
    private ImageView photoIcon, cancel, save;
    private List<Budget> budgets;
    private TextView location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deal_edit);

        budgetRepository = new BudgetRepository(getApplication());
        dealRepository = new DealRepository(getApplication());
        extras = getIntent().getExtras();

        initViews();
        setSpinners();
        fillFields();
        setListeners();
    }

    private void setSpinners() {
        budgetRepository.getmBudgets().observe(this, budgets -> {
            this.budgets = budgets;
            ArrayAdapter<String> budgetAdapter = new ArrayAdapter<String>(
                    this,
                    R.layout.support_simple_spinner_dropdown_item,
                    budgets.stream().map(Budget::getName).collect(Collectors.toList()));
            budgetAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
            budgetSpinner.setAdapter(budgetAdapter);
            Budget bundleBudget = (Budget) extras.getSerializable("budget");
            Budget currentBudget = budgets.stream().filter(budget -> budget.getId().equals(bundleBudget.getId())).findFirst().get();
            int index = budgets.indexOf(currentBudget);
            budgetSpinner.setSelection(index);
        });
        budgetRepository.getAll(new BudgetQuery());

        budgetSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (!budgetSpinner.getSelectedItem().toString().equals("<Add budget>"))
//                viewModel.setNewBudget(budgetSpinner.getSelectedItem().toString());
                Budget selectedBudget = budgets.stream().filter(budget -> budget.getName().equals(budgetSpinner.getSelectedItem().toString())).findFirst().get();
                Snackbar.make(findViewById(R.id.deal_edit_layout), "Selected budget's balance: " + selectedBudget.getDeals().stream().mapToDouble(Deal::getValue).sum(), Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        String dealtype = extras.getString("dealType");
        if (dealtype.toUpperCase().equals("INCOME")) dealTypeSpinner.setSelection(0);
        else dealTypeSpinner.setSelection(1);

        String dealsource = extras.getString("dealSource");
        dealsource = dealsource.toUpperCase();
        List<String> dealsources = Arrays.asList(getResources().getStringArray(R.array.dealsources));
        dealsource = getDealsourceInLocaleLanguage(dealsource);
        int index = dealsources.indexOf(dealsource);
        dealSourceSpinner.setSelection(index);

    }

    private String getDealsourceInLocaleLanguage(String dealsource){
        String translated = dealsource;
        String locale = getResources().getConfiguration().locale.getDisplayLanguage(new Locale("en"));
        if (!locale.equals("Polish")){
            if (dealsource.equals("CZAS WOLNY")) translated = "FREETIME";
            else if (dealsource.equals("ELEKTRONIKA")) translated = "ELECTRONICS";
            else if (dealsource.equals("HOBBY")) translated = "HOBBY";
            else if (dealsource.equals("JEDZENIE")) translated = "FOOD";
            else if (dealsource.equals("NAPRAWA")) translated = "REPAIR";
            else if (dealsource.equals("PALIWO")) translated = "FUEL";
            else if (dealsource.equals("PODATEK")) translated = "TAX";
            else if (dealsource.equals("PRACA")) translated = "WORK";
            else if (dealsource.equals("PREZENT")) translated = "GIFT";
            else if (dealsource.equals("RATA")) translated = "INSTALMENT";
            else if (dealsource.equals("SUBSKRYPCJA")) translated = "SUBSCRIPTION";
            else if (dealsource.equals("SZKOŁA")) translated = "SCHOOL";
            else if (dealsource.equals("TRANSPORT")) translated = "TRANSPORTATION";
            else if (dealsource.equals("UBEZPIECZENIE")) translated = "INSURANCE";
            else if (dealsource.equals("UBRANIA")) translated = "CLOTHES";
            else if (dealsource.equals("UNIWERSYTET")) translated = "UNIVERSITY";
            else if (dealsource.equals("INNE")) translated = "OTHER";
        }
        else{
            if (dealsource.equals("CLOTHING")) translated = "UBRANIA";
            else if (dealsource.equals("ELECTRONICS")) translated = "ELEKTRONIKA";
            else if (dealsource.equals("FOOD")) translated = "JEDZENIE";
            else if (dealsource.equals("FREETIME")) translated = "CZAS WOLNY";
            else if (dealsource.equals("FUEL")) translated = "PALIWO";
            else if (dealsource.equals("GIFT")) translated = "PREZENT";
            else if (dealsource.equals("HOBBY")) translated = "HOBBY";
            else if (dealsource.equals("INSTALMENT")) translated = "RATA";
            else if (dealsource.equals("INSURANCE")) translated = "UBEZPIECZENIE";
            else if (dealsource.equals("REPAIR")) translated = "NAPRAWA";
            else if (dealsource.equals("SCHOOL")) translated = "SZKOŁA";
            else if (dealsource.equals("SUBSCRIPTION")) translated = "SUBSKRYPCJA";
            else if (dealsource.equals("TAX")) translated = "TAX";
            else if (dealsource.equals("TRANSPORTATION")) translated = "TRANSPORT";
            else if (dealsource.equals("UNIVERSITY")) translated = "UNIWERSYTET";
            else if (dealsource.equals("WORK")) translated = "PRACA";
            else if (dealsource.equals("OTHER")) translated = "INNE";
        }
        return translated;
    }

//    @Override
//    public void onBackPressed() {
//        exitActivity();
//    }

    private void setListeners() {
        cancel.setOnClickListener(v -> {
            exitActivity();
        });

        save.setOnClickListener(v -> {
            updateDeal();
        });

        photoIcon.setOnClickListener(v -> {
            Deal deal = (Deal) extras.getSerializable("deal");
            if (deal.getFile() == null) {
                Toast.makeText(this, R.string.empty, Toast.LENGTH_SHORT).show();
                return;
            }
//            dealRepository.getmFile().observe(this, bytes -> {
            Intent intent = new Intent(this, ImageViewerActivity.class);
//                BitmapFactory.Options options = new BitmapFactory.Options();
//                options.inMutable = true;
//                Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options);
//                intent.putExtra("BitmapImage", bytes);
            intent.putExtra("downloadId", deal.getId());
            startActivity(intent);
//            });


//            dealRepository.downloadFile(deal.getId());

        });

        location.setOnClickListener(v -> {
            if (extras.getDouble("latitude", 200) < 200) {
                String intentStr = "geo:0,0?q=" + extras.getDouble("latitude") + "," + extras.getDouble("longitude");
//            Uri gmmIntentUri = Uri.parse("geo:0,0?q=53.1324886,23.1688403");
                Uri gmmIntentUri = Uri.parse(intentStr);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                if (mapIntent.resolveActivity(getPackageManager()) != null && mapIntent.setPackage("com.google.android.apps.maps") != null) {
                    startActivity(mapIntent);
                }
            } else Toast.makeText(this, R.string.empty, Toast.LENGTH_SHORT).show();
        });
    }

    private void updateDeal() {
        DealRequest dealRequest = new DealRequest();

        Long budgetId = budgets.get(budgetSpinner.getSelectedItemPosition()).getId();

        String dealsource = dealSourceSpinner.getSelectedItem().toString().toUpperCase();
        String dealtype = dealTypeSpinner.getSelectedItem().toString().toUpperCase();
        String translated = "CLOTHES";
        if (dealsource.equals("CZAS WOLNY")) translated = "FREETIME";
        else if (dealsource.equals("ELEKTRONIKA")) translated = "ELECTRONICS";
        else if (dealsource.equals("HOBBY")) translated = "HOBBY";
        else if (dealsource.equals("JEDZENIE")) translated = "FOOD";
        else if (dealsource.equals("NAPRAWA")) translated = "REPAIR";
        else if (dealsource.equals("PALIWO")) translated = "FUEL";
        else if (dealsource.equals("PODATEK")) translated = "TAX";
        else if (dealsource.equals("PRACA")) translated = "WORK";
        else if (dealsource.equals("PREZENT")) translated = "GIFT";
        else if (dealsource.equals("RATA")) translated = "INSTALMENT";
        else if (dealsource.equals("SUBSKRYPCJA")) translated = "SUBSCRIPTION";
        else if (dealsource.equals("SZKOŁA")) translated = "SCHOOL";
        else if (dealsource.equals("TRANSPORT")) translated = "TRANSPORTATION";
        else if (dealsource.equals("UBEZPIECZENIE")) translated = "INSURANCE";
        else if (dealsource.equals("UBRANIA")) translated = "CLOTHES";
        else if (dealsource.equals("UNIWERSYTET")) translated = "UNIVERSITY";
        else if (dealsource.equals("INNE")) translated = "OTHER";

        if (dealtype.equals("WYDATEK")) dealtype = "EXPENSE";
        else if (dealtype.equals("ZAROBEK")) dealtype = "INCOME";

        dealRequest.setId(extras.getLong("id"));
        dealRequest.setValue(Double.valueOf(value.getText().toString()));
        dealRequest.setNote(note.getText().toString());
        dealRequest.setBudgetId(budgetId);
        dealRequest.setDealSource(translated);
        dealRequest.setDealType(dealtype);

        dealRepository.update(dealRequest);

    }

    private void exitActivity() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.exit_confirmation_unsaved_changes)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, (dialog, id) -> {
                    this.finish();
                })
                .setNegativeButton(R.string.no, (dialog, id) -> {
                    dialog.cancel();
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void fillFields() {
        long _id = extras.getLong("id");
        String _note = extras.getString("note");
        double _value = extras.getDouble("value");
        long _budgetId = extras.getLong("budgetId");
        String _filePath = extras.getString("filePath");
        LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.parse(extras.getString("datetime")), ZoneId.systemDefault());
        String _dealType = extras.getString("dealType");
        String _dealSource = extras.getString("dealSource");
        Budget budget = (Budget) extras.getSerializable("budget");
        Deal deal = (Deal) extras.getSerializable("deal");
        Log.v("tag", budget.getName());

        String dateStr = localDateTime.getDayOfMonth() + " - " +
                localDateTime.getMonthValue() + " - " +
                localDateTime.getYear() + " " +
                localDateTime.getHour() + ":" +
                localDateTime.getMinute();

        date.setText(dateStr);
        note.setText(_note);
        value.setText(String.valueOf(Math.abs(_value)));
    }

    private void initViews() {
        date = findViewById(R.id.deal_edit_date);
        note = findViewById(R.id.deal_edit_note);
        value = findViewById(R.id.deal_edit_value);
        budgetSpinner = findViewById(R.id.deal_edit_budget_spinner);
        dealTypeSpinner = findViewById(R.id.deal_edit_dealtype_spinner);
        dealSourceSpinner = findViewById(R.id.deal_edit_dealsource_spinner);
        changePhoto = findViewById(R.id.deal_edit_photo_button);
        photoIcon = findViewById(R.id.deal_edit_photo_icon);
        cancel = findViewById(R.id.deal_edit_cancel_button);
        save = findViewById(R.id.deal_edit_save_button);
        location = findViewById(R.id.deal_edit_location_button);
    }


}