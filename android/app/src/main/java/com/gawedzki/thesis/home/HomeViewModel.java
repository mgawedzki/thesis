package com.gawedzki.thesis.home;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import com.gawedzki.thesis.budget.Budget;
import com.gawedzki.thesis.budget.BudgetQuery;
import com.gawedzki.thesis.budget.BudgetRepository;
import com.gawedzki.thesis.deal.Deal;
import com.gawedzki.thesis.deal.DealRepository;
import com.gawedzki.thesis.deal.DealRequest;
import com.gawedzki.thesis.location.Location;

import java.io.File;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

public class HomeViewModel extends AndroidViewModel {

    private BudgetRepository budgetRepository;
    private DealRepository dealRepository;
    private LiveData<List<Budget>> budgets;
    private LiveData<List<String>> budgetsNames;
    private MutableLiveData<Long> mBudgetId = new MutableLiveData<>();
    private LiveData<Budget> budget/* = Transformations.switchMap(mBudgetName, name -> budgetRepository.getBudgetByName(name))*/;
    private LiveData<Double> balance/* = Transformations.switchMap(mBudgetName, input -> calculateBalance())*/;
    private MutableLiveData<Bitmap> mBitmap = new MutableLiveData<>();
    private MutableLiveData<Deal> mDeal = new MutableLiveData<>();
    private MutableLiveData<Location> mLocation = new MutableLiveData<>();
    private LiveData<Double> mLastNdaysBalance = new MutableLiveData<>();
    private int n_days;

    public HomeViewModel(@NonNull Application application) {
        super(application);
        SharedPreferences sharedPrefs = application.getApplicationContext().getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE);
        n_days = sharedPrefs.getInt("default_last_n_days", 7);
        budgetRepository = new BudgetRepository(application);
        dealRepository = new DealRepository(application);
        budgets = budgetRepository.getmBudgets();
        budgetsNames = Transformations.map(budgets, input -> input.stream().map(Budget::getName).collect(Collectors.toList()));
        budget = Transformations.map(
                mBudgetId, name -> budgets
                        .getValue()
                        .stream()
                        .filter(selectedBudged -> selectedBudged.getId().equals(mBudgetId.getValue()))
                        .findFirst()
                        .get());
        mLastNdaysBalance = Transformations.switchMap(mBudgetId, input -> calculateNDays());
        balance = Transformations.switchMap(mBudgetId, input -> calculateBalance());
        if (budgets.getValue() != null) {
            mBudgetId.setValue(budgets.getValue().get(0).getId());
        }
        Transformations.switchMap(mBudgetId, input -> {
            Log.v("tag", "calculateNDays");
            budgetRepository.getlastNdaysBalance(mBudgetId.getValue(), n_days);
            return null;
        });
        syncBudgets();
    }

    public MutableLiveData<Location> getmLocation() {
        return mLocation;
    }

    public void setmLocation(Location location) {
        this.mLocation.setValue(location);
    }


    public LiveData<List<String>> getBudgetsNames() {
        assert budgets != null : "budgets is null";
        budgetsNames = Transformations.map(budgets, input -> input.stream().map(Budget::getName).collect(Collectors.toList()));
        return budgetsNames;
    }

    private LiveData<Double> calculateNDays() {
        Log.v("tag", "calculateNDays");
        return Transformations.map(budget, input -> budgetRepository.getlastNdaysBalance(budget.getValue().getId(), n_days).getValue()
        );
    }

    private LiveData<Double> calculateBalance() {
        Log.v("tag", "calculateBalance");
        return Transformations.map(budget, input -> input.getDeals().stream().mapToDouble(Deal::getValue).sum());
    }

    public LiveData<Budget> getBudget() {
        return budget;
    }

    public LiveData<List<Budget>> getBudgets() {
        return budgets;
    }

    public LiveData<Double> getBalance() {
        return balance;
    }

    public void setNewBudget(String name) {
        Budget budget = budgets.getValue().stream().filter(budget1 -> budget1.getName().equals(name)).findFirst().get();
        this.mBudgetId.setValue(budget.getId());
    }

    public LiveData<Bitmap> getBitmap() {
        assert this.mBitmap != null : "bitmap is null";
        return mBitmap;
    }

    public void setBitmap(Bitmap mBitmap) {
        assert this.mBitmap != null : "bitmap is null";
        this.mBitmap.setValue(mBitmap);
    }

    public void syncBudgets() {
        budgetRepository.getAll(new BudgetQuery());
        updateData();
    }

    private void updateData() {
        budgetsNames = Transformations.map(budgets, input -> input.stream().map(Budget::getName).collect(Collectors.toList()));
    }

    public void saveDeal(Deal deal) {
        DealRequest request = new DealRequest();

        request.budgetName = budget.getValue().getName();
        request.note = deal.getNote();
        request.dealType = deal.getDealType();
        request.dealSource = deal.getDealSource();
        request.value = deal.getValue();
        request.filePath = deal.getFilePath();
        request.datetime = deal.getDatetime();
        if (request.datetime == null) request.datetime = Instant.now();
        if (mLocation.getValue() != null) {
            request.latitude = mLocation.getValue().getLatitude();
            request.longitude = mLocation.getValue().getLongitude();
        }
        File file = null;
        if (request.getFilePath() != null) {
            file = new File(request.getFilePath());
        }
        request.setFile(file);

        dealRepository.save(request);
        syncBudgets();
    }

    public void getNDays(Long id, int n) {
        Log.v("tag", "calculateNDays");
        budgetRepository.getlastNdaysBalance(budget.getValue().getId(), n_days);
    }

    public MutableLiveData<Double> getmLastNdaysBalance() {
        return budgetRepository.getmLastNdaysBalance();
    }

//    public void setmLastNdaysBalance(int n) {
//        mLastNdaysBalance.setValue(budgetRepository.getmLastNdaysBalance().getValue());
//    }
}
