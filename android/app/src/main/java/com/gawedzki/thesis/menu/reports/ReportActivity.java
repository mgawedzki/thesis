package com.gawedzki.thesis.menu.reports;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.gawedzki.thesis.R;
import com.gawedzki.thesis.budget.Budget;
import com.gawedzki.thesis.deal.Deal;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

public class ReportActivity extends AppCompatActivity {

    ReportViewModel viewModel;
    BarChart barChart;
    Spinner budgetNamesSpinner;
    TextView dailyAverage;

    List<Deal> deals;
    List<Budget> budgets;

    final ArrayList<String> xAxisLabel = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
//        sampleChart(R.id.report_bar_chart);
        initViews();
        viewModel = ViewModelProviders.of(this).get(ReportViewModel.class);

        viewModel.getmBudgets().observe(this, vmBudgets -> {
            viewModel.getmSelectedBudget().setValue(vmBudgets.get(0));
            budgets = vmBudgets;
            setBudgetSpinner(budgets.stream().map(Budget::getName).collect(Collectors.toList()));
//            doChart(budgets.get(0).getDeals());
        });

        viewModel.getmDeals().observe(this, vmDeals -> {
            deals = vmDeals;
        });

        viewModel.getmSelectedBudget().observe(this, budget -> {
            SharedPreferences sharedPrefs = getApplicationContext().getSharedPreferences("sharedPrefs", MODE_PRIVATE);
            int n_days = sharedPrefs.getInt("default_last_n_days", 7);
            List<Deal> deals = budget.getDeals().stream()
                    .filter(deal -> deal.getDatetime().isAfter(Instant.now().minus(n_days, ChronoUnit.DAYS)))
                    .collect(Collectors.toList());

            double average, sum = 0;
            String sign = " +";
            float[] floats = calculateMoneyPerDayArray(n_days, deals);
            for (float aFloat : floats) {
                sum += aFloat;
            }
            average = sum / floats.length;

            if (average < 0) sign = " -";

            String currency = budget.getCurrency().getSymbol(new Locale(sharedPrefs.getString("default_locale", "en")));
            dailyAverage.setText(getResources().getString(R.string.daily_average_balance) + sign + String.format("%.2f", average) + "(" + currency + ")");
            doChart(deals);
        });

        budgetNamesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                viewModel.setmSelectedBudget(budgets.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        viewModel.updateBudgets();
        viewModel.updateDeals();
    }

    private void setBudgetSpinner(List<String> budgetsNames) {
        ArrayAdapter<String> budgetAdapter = new ArrayAdapter<String>(
                this,
                R.layout.support_simple_spinner_dropdown_item,
                budgetsNames);
        budgetNamesSpinner.setAdapter(budgetAdapter);
    }

    private void doChart(List<Deal> dealsToDraw) {
        int n_days = getApplicationContext().getSharedPreferences("sharedPrefs", MODE_PRIVATE).getInt("default_last_n_days", 7);
        float[] sumArr = new float[n_days];
        ArrayList<BarEntry> visitors = new ArrayList<>();

        dealsToDraw = filterDealsAfter(n_days, dealsToDraw);
        sumArr = calculateMoneyPerDayArray(n_days, dealsToDraw);

        for (int i = 0; i < sumArr.length; i++) {
            visitors.add(new BarEntry(-i, sumArr[i]));
        }

        BarDataSet barDataSet = new BarDataSet(visitors, getResources().getString(R.string.money_spent));
        barDataSet.setColors(ColorTemplate.MATERIAL_COLORS);
        barDataSet.setValueTextSize(16f);

        BarData barData = new BarData(barDataSet);

        barChart.setFitBars(true);
        barChart.setData(barData);
        barChart.getDescription().setText(getResources().getString(R.string.last_n_days, n_days));
        barChart.animateY(1000);

        SharedPreferences sharedPreferences = getSharedPreferences("sharedPrefs", MODE_PRIVATE);
        int modeNightYes = AppCompatDelegate.MODE_NIGHT_YES;
        int theme = sharedPreferences.getInt("mytheme", modeNightYes);
        if (theme == modeNightYes) {
            barChart.getAxisLeft().setTextColor(Color.WHITE);
            barChart.getAxisRight().setTextColor(Color.WHITE);
            barChart.getXAxis().setTextColor(Color.WHITE);
            barChart.getDescription().setTextColor(Color.WHITE);
            barDataSet.setValueTextColor(Color.WHITE);
        } else {
            barChart.getAxisLeft().setTextColor(Color.BLACK);
            barChart.getAxisRight().setTextColor(Color.BLACK);
            barChart.getXAxis().setTextColor(Color.BLACK);
            barChart.getDescription().setTextColor(Color.BLACK);
            barDataSet.setValueTextColor(Color.BLACK);
        }
    }

    private List<Deal> filterDealsAfter(int n_days, List<Deal> deals) {
        deals = deals.stream()
                .filter(deal -> deal.getDatetime().isAfter(Instant.now().minus(n_days, ChronoUnit.DAYS)))
                .sorted((o1, o2) -> o1.getDatetime().compareTo(o2.getDatetime()))
                .collect(Collectors.toList());
        return deals;
    }

    private float[] calculateMoneyPerDayArray(int days, List<Deal> deals) {
        float[] sumArr = new float[days];
        for (Deal deal : deals) {
            long between = ChronoUnit.DAYS.between(deal.getDatetime(), Instant.now());
//            if (deal.getDealType().equals("INCOME"))
                sumArr[(int) between] += deal.getValue();
//            else sumArr[(int) between] -= deal.getValue();
        }
        return sumArr;
    }

    private void initViews() {
        barChart = findViewById(R.id.report_bar_chart);
        budgetNamesSpinner = findViewById(R.id.report_budget_spinner);
        dailyAverage = findViewById(R.id.report_average);
    }

    private void sampleChart(int chartId) {
        barChart = findViewById(chartId);
        ArrayList<BarEntry> visitors = new ArrayList<>();
        visitors.add(new BarEntry(2020, 470));
        visitors.add(new BarEntry(2019, 630));
        visitors.add(new BarEntry(2018, 550));
        visitors.add(new BarEntry(2017, 660));
        visitors.add(new BarEntry(2016, 508));
        visitors.add(new BarEntry(2015, 475));
        visitors.add(new BarEntry(2014, 420));
        visitors.add(new BarEntry(2014, 420));
        visitors.add(new BarEntry(2014, 420));

        BarDataSet barDataSet = new BarDataSet(visitors, "Visitors");
        barDataSet.setColors(ColorTemplate.JOYFUL_COLORS);
        barDataSet.setValueTextColor(Color.WHITE);
        barDataSet.setValueTextSize(16f);

        BarData barData = new BarData(barDataSet);

        barChart.setFitBars(true);
        barChart.setData(barData);
        barChart.getDescription().setText("Bar Chart Example");
        barChart.animateY(2000);
    }
}