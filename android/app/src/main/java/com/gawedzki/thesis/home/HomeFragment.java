package com.gawedzki.thesis.home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
//import androidx.lifecycle.ViewModelProviders;

import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.gawedzki.thesis.MainActivity;
import com.gawedzki.thesis.R;
import com.gawedzki.thesis.budget.Budget;
import com.gawedzki.thesis.deal.Deal;
import com.gawedzki.thesis.image.ImageViewerActivity;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.tasks.Task;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;
import java.util.stream.Collectors;

import static android.content.Context.MODE_PRIVATE;

public class HomeFragment extends Fragment {

    static final int REQUEST_IMAGE_CAPTURE = 1;

    private HomeViewModel viewModel;
    private RadioGroup dealTypeRadio;
    private String dealType;
    private AppCompatRadioButton incomeButton, expenseButton;
    private TextView topText, topBalanceValue, clear, save, datePicker, timePicker;
    private Spinner dealSourceSpinner, budgetSpinner;
    private TextView photoButton;
    private ImageView topView, fullScreen, sync;
    private EditText note, description, value;
    private Currency currentCurrency;
    private CheckBox locationCheckBox;
    String currentPhotoPath;
    Uri photoURI;
    FusedLocationProviderClient locationClient;
    LocationManager locationManager;

    public HomeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        viewModel = new ViewModelProvider(requireActivity(), getDefaultViewModelProviderFactory()).get(HomeViewModel.class);
//        viewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
//        viewModel = new HomeViewModel(getActivity().getApplication());
//        viewModel = new ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory.getInstance(this.getActivity().getApplication())).get(HomeViewModel.class);
        viewModel = new ViewModelProvider(this).get(HomeViewModel.class);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
//            Bundle extras = data.getExtras();
//            Bitmap imageBitmap = (Bitmap) extras.get("data");
//            topView.setImageBitmap(imageBitmap);
//            topView.setColorFilter(Color.rgb(50, 50, 50), PorterDuff.Mode.MULTIPLY);
//            viewModel.setBitmap(imageBitmap);
//            fullScreen.setVisibility(View.VISIBLE);
//            dispatchTakePictureIntent();
//        }

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            File file = new File(photoURI.getPath());
            Bitmap photoBitmap = BitmapFactory.decodeFile(currentPhotoPath);
            topView.setImageBitmap(photoBitmap);
            topView.setColorFilter(Color.rgb(50, 50, 50), PorterDuff.Mode.MULTIPLY);
            viewModel.setBitmap(photoBitmap);
            fullScreen.setVisibility(View.VISIBLE);
        }

    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "THESIS_JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpeg",   /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that theres a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile;
            try {
                photoFile = createImageFile();
            } catch (IOException e) {
                // Error occured while creating the File
                e.printStackTrace();
                return;
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(
                        getContext(),
                        "com.gawedzki.thesis",
                        photoFile
                );
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private void initViews() {
        findViews();

        dealType = getResources().getString(R.string.income);

        SharedPreferences sharedPrefs = this.getActivity().getSharedPreferences("sharedPrefs", MODE_PRIVATE);
        String defaultDealtype = sharedPrefs.getString("default_dealtype", getResources().getString(R.string.income));

        dealType = defaultDealtype;
        if (defaultDealtype.equals(getResources().getString(R.string.income))) {
            dealTypeRadio.check(R.id.left_radio);
        } else dealTypeRadio.check(R.id.right_radio);


//        List<String> budgetsNames = new LinkedList<>();
//        budgetsNames.add("<Add budget>");
//        ArrayAdapter<String> budgetAdapter = new ArrayAdapter<>(getContext(), R.layout.support_simple_spinner_dropdown_item, budgetsNames);
//        budgetAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
//        budgetSpinner.setAdapter(budgetAdapter);

        ArrayAdapter<CharSequence> dealSourceAdapter = ArrayAdapter.createFromResource(getContext(), R.array.dealsources, R.layout.support_simple_spinner_dropdown_item);
        dealSourceAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        dealSourceSpinner.setAdapter(dealSourceAdapter);

        Calendar _calendar = Calendar.getInstance();
        int _year = _calendar.get(Calendar.YEAR);
        int _month = _calendar.get(Calendar.MONTH);
        int _dayOfMonth = _calendar.get(Calendar.DAY_OF_MONTH);
        int _hour = _calendar.get(Calendar.HOUR_OF_DAY);
        int _minute = _calendar.get(Calendar.MINUTE);
        datePicker.setText(LocalDate.of(_year, _month + 1, _dayOfMonth).toString());
        timePicker.setText(String.format("%02d", _hour) + ":" + String.format("%02d", _minute));


        save.setOnClickListener(v -> {
            saveDeal();
        });

        sync.setOnClickListener(v -> {
            viewModel.syncBudgets();
        });

        clear.setOnClickListener(v -> {
            MainActivity mainActivity = (MainActivity) getActivity();
            assert mainActivity != null;
            mainActivity.setHomeFragment(new HomeFragment());
        });

        note.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) v.setBackgroundResource(R.drawable.round_border_red);
            else v.setBackgroundResource(0);
        });

        value.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) v.setBackgroundResource(R.drawable.round_border_red);
            else v.setBackgroundResource(0);
        });

        budgetSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (!budgetSpinner.getSelectedItem().toString().equals("<Add budget>"))
                viewModel.setNewBudget(budgetSpinner.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        photoButton.setOnClickListener(v -> {
//            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//            try {
//                startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
//            } catch (ActivityNotFoundException e) {
//                e.printStackTrace();
//            }
            dispatchTakePictureIntent();
        });

        topBalanceValue.setOnClickListener(v -> {
            int n_days = sharedPrefs.getInt("default_last_n_days", 7);
            if (topText.getText().equals(getResources().getString(R.string.last_n_days, n_days))) {
                topText.setText(R.string.balance);

                topBalanceValue.setText(String.format("%.2f",
                        viewModel.getBalance().getValue()) +
                        " " +
                        viewModel.getBudget()
                                .getValue()
                                .getCurrency()
                                .getSymbol(new Locale(sharedPrefs.getString("default_locale", "en"))));
            } else {
                topText.setText(getResources().getString(R.string.last_n_days, n_days));

                Double value = viewModel.getmLastNdaysBalance().getValue();
                if (value == null) value = (double) 0;
                topBalanceValue.setText(String.format("%.2f", value) +
                        " " +
                        viewModel.getBudget()
                                .getValue()
                                .getCurrency()
                                .getSymbol(new Locale(sharedPrefs.getString("default_locale", "en"))));

            }
        });

        dealTypeRadio.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.left_radio:
                    dealType = getResources().getString(R.string.income);
                    incomeButton.setTypeface(null, Typeface.BOLD);
                    expenseButton.setTypeface(null, Typeface.NORMAL);
                    break;
                case R.id.right_radio:
                    dealType = getResources().getString(R.string.expense);
                    incomeButton.setTypeface(null, Typeface.NORMAL);
                    expenseButton.setTypeface(null, Typeface.BOLD);
                    break;
            }
        });

        fullScreen.setOnClickListener(v -> {
            if (currentPhotoPath == null || currentPhotoPath.isEmpty()) {
                Toast.makeText(getContext(), R.string.empty, Toast.LENGTH_SHORT).show();
                return;
            }
            Intent intent = new Intent(getContext(), ImageViewerActivity.class);
            intent.putExtra("filepath", currentPhotoPath);
            startActivity(intent);
        });

        datePicker.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    datePicker.setText(LocalDate.of(year, month + 1, dayOfMonth).toString());
                }
            }, year, month, dayOfMonth);
            dialog.show();
        });

        timePicker.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);
            TimePickerDialog dialog;
            dialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    timePicker.setText(String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute));
                }
            }, hour, minute, true);
            dialog.show();
        });

        locationCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> {

            LocationManager lm = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
            if (!lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                locationCheckBox.setChecked(false);

                boolean gps_enabled = false;
                boolean network_enabled = false;

                try {
                    gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
                } catch(Exception ex) {}

                try {
                    network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                } catch(Exception ex) {}

                if(!gps_enabled && !network_enabled) {
                    locationCheckBox.setChecked(false);
                    // notify user
                    new AlertDialog.Builder(getContext())
                            .setMessage(R.string.location_is_disabled)
                            .setPositiveButton(R.string.open_location_settings, (paramDialogInterface, paramInt) -> getContext().startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)))
                            .setNegativeButton("Cancel",null)
                            .show();
                }

                if (!locationCheckBox.isChecked()) return;
            }

            if (isChecked) {
//                Toast.makeText(getContext(), R.string.establishing_your_location, Toast.LENGTH_SHORT).show();
                locationCheckBox.setText(R.string.establishing_your_location);
                locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10, 1, new LocationListener() {
                        @Override
                        public void onLocationChanged(@NonNull Location location) {
                            com.gawedzki.thesis.location.Location myLocation = new com.gawedzki.thesis.location.Location(location.getLatitude(), location.getLongitude());
                            viewModel.getmLocation().setValue(myLocation);
//                            Toast.makeText(getContext(), R.string.location_established, Toast.LENGTH_SHORT).show();
                            locationCheckBox.setText(R.string.location_established);
                        }
                    });
                } else {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 44);
                }
            } else {
                locationCheckBox.setText(R.string.location);
            }
        });

        boolean defaultLocation = sharedPrefs.getBoolean("default_location", false);
        int defaultDealsource = sharedPrefs.getInt("default_dealsource", 0);

        locationCheckBox.setChecked(defaultLocation);
        dealSourceSpinner.setSelection(defaultDealsource, true);

    }

    private void getCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Task<Location> task = locationClient.getLastLocation();
        task.addOnSuccessListener(location -> {
            if (location != null) {
                Log.v("tag", "location " + location.getLongitude() + " " + location.getLatitude());
                com.gawedzki.thesis.location.Location myLocation = new com.gawedzki.thesis.location.Location();
                myLocation.setLatitude(location.getLatitude());
                myLocation.setLongitude(location.getLongitude());
                Toast.makeText(getContext(), R.string.location_established, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 44) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getCurrentLocation();
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();

        SharedPreferences sharedPrefs = this.getActivity().getSharedPreferences("sharedPrefs", MODE_PRIVATE);
        int n_days = sharedPrefs.getInt("default_last_n_days", 7);
        viewModel.getmLastNdaysBalance()
                .observe(getViewLifecycleOwner(), aDouble -> {
//                    if (topText.getText().toString().startsWith("Last"))
//                    topBalanceValue.setText(String.format("%.2f", aDouble));
                });


        viewModel.getBudget()
                .observe(getViewLifecycleOwner(), budget -> {
                    topBalanceValue.setText(budget.getName());
                    viewModel.getNDays(budget.getId(), n_days);
                });

        viewModel.getBudgets()
                .observe(getViewLifecycleOwner(), budgets -> {
                    ArrayAdapter<String> budgetAdapter = new ArrayAdapter<String>(
                            getContext(),
                            R.layout.support_simple_spinner_dropdown_item,
                            budgets.stream().map(Budget::getName).collect(Collectors.toList()));
                    budgetAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                    budgetSpinner.setAdapter(budgetAdapter);
                });

        viewModel.getBalance()
                .observe(getViewLifecycleOwner(), aDouble -> {
                    topBalanceValue.setText(String.format("%.2f", viewModel.getBalance().getValue()) +
                            " " +
                            viewModel.getBudget()
                                    .getValue()
                                    .getCurrency()
                                    .getSymbol(new Locale(sharedPrefs.getString("default_locale", "en"))));
                });

        viewModel.getBitmap()
                .observe(getViewLifecycleOwner(), bitmap -> {
                    topView.setImageBitmap(bitmap);
                });


//        viewModel.getBudgetsNames()
//                .observe(getViewLifecycleOwner(), budgetsNames -> {
//                    if (budgetsNames == null || budgetsNames.size() == 0) {
//                        budgetsNames.add("<Add budget>");
//                    }
//                    ArrayAdapter<String> budgetAdapter = new ArrayAdapter<>(getContext(), R.layout.support_simple_spinner_dropdown_item, budgetsNames);
//                    budgetAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
//                    budgetSpinner.setAdapter(budgetAdapter);
//                });

    }

    @NonNull
    private Resources getLocalizedResources(Context context, Locale desiredLocale) {
        Configuration conf = context.getResources().getConfiguration();
        conf = new Configuration(conf);
        conf.setLocale(desiredLocale);
        Context localizedContext = context.createConfigurationContext(conf);
        return localizedContext.getResources();
    }

    private void findViews() {
        dealTypeRadio = getView().findViewById(R.id.radio_group);
        incomeButton = getView().findViewById(R.id.left_radio);
        expenseButton = getView().findViewById(R.id.right_radio);
        topText = getView().findViewById(R.id.top_text);
        topBalanceValue = getView().findViewById(R.id.top_balance_value_textview);
        dealSourceSpinner = getView().findViewById(R.id.deal_source_spinner);
        photoButton = getView().findViewById(R.id.photo_button);
        budgetSpinner = getView().findViewById(R.id.budget_spinner);
        topView = getView().findViewById(R.id.top_view);
        note = getView().findViewById(R.id.note_edit_text);
//        description = getView().findViewById(R.id.description_edit_text);
        value = getView().findViewById(R.id.value_edit_text);
        fullScreen = getView().findViewById(R.id.full_screen_button);
        clear = getView().findViewById(R.id.clear_button);
        save = getView().findViewById(R.id.save_button);
        sync = getView().findViewById(R.id.sync_button);
        datePicker = getView().findViewById(R.id.date_picker);
        timePicker = getView().findViewById(R.id.time_picker);
        locationCheckBox = getView().findViewById(R.id.location_check_box);
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void saveDeal() {

        if (value.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), R.string.there_are_empty_fields, Toast.LENGTH_SHORT).show();
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(R.string.save_transaction_q)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, (dialog, id) -> {
                    ((MainActivity) getActivity()).showLoadingDialog();
                    Deal deal = new Deal();
                    deal.setNote(note.getText().toString());
                    String dateString = datePicker.getText().toString();
                    String timeString = timePicker.getText().toString();
                    LocalDate localDate = LocalDate.parse(dateString);
                    LocalTime localTime = LocalTime.parse(timeString, DateTimeFormatter.ofPattern("HH:mm"));
                    LocalDateTime localDateTime = LocalDateTime.of(localDate, localTime);
                    Instant instant = localDateTime.atZone(ZoneId.systemDefault()).toInstant();
                    deal.setDatetime(instant);

                    String dealSource = dealSourceSpinner.getSelectedItem().toString();
                    String translateDealsource = dealSourceToEnglish(dealSource);
                    deal.setDealSource(translateDealsource);

                    if (dealType.equals(getResources().getString(R.string.income))) {
                        deal.setDealType(getLocalizedResources(getContext(), new Locale("en")).getString(R.string.income));
                    } else
                        deal.setDealType(getLocalizedResources(getContext(), new Locale("en")).getString(R.string.expense));


                    Double value;
                    if (dealType.equals(getResources().getString(R.string.income)))
                        value = Double.valueOf(this.value.getText().toString());
                    else
                        value = Double.valueOf("-" + this.value.getText().toString());

                    deal.setValue(value);
                    if (currentPhotoPath != null) {
                        deal.setFilePath(currentPhotoPath);
                        deal.setFile(new File(currentPhotoPath));
                    }
                    viewModel.saveDeal(deal);

                    MainActivity mainActivity = (MainActivity) getActivity();
                    mainActivity.setHomeFragment(new HomeFragment());
                })
                .setNegativeButton(R.string.no, (dialog, id) -> {
                    dialog.cancel();
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private String dealSourceToEnglish(String dealSource) {
        String locale = getResources().getConfiguration().locale.getDisplayLanguage(new Locale("en"));
        if (!locale.equals("Polish")) return dealSource;

        switch (dealSource) {
            case "CZAS WOLNY":
                dealSource = "FREETIME";
                break;
            case "ELEKTRONIKA":
                dealSource = "ELECTRONICS";
                break;
            case "HOBBY":
                dealSource = "HOBBY";
                break;
            case "JEDZENIE":
                dealSource = "FOOD";
                break;
            case "NAPRAWA":
                dealSource = "REPAIR";
                break;
            case "PALIWO":
                dealSource = "FUEL";
                break;
            case "PODATEK":
                dealSource = "TAX";
                break;
            case "PRACA":
                dealSource = "WORK";
                break;
            case "PREZENT":
                dealSource = "GIFT";
                break;
            case "RATA":
                dealSource = "INSTALMENT";
                break;
            case "SUBSKRYPCJA":
                dealSource = "SUBSCRIPTION";
                break;
            case "SZKOŁA":
                dealSource = "SCHOOL";
                break;
            case "TRANSPORT":
                dealSource = "TRANSPORTATION";
                break;
            case "UBEZPIECZENIE":
                dealSource = "INSURANCE";
                break;
            case "UBRANIA":
                dealSource = "CLOTHING";
                break;
            case "UNIWERSYTET":
                dealSource = "UNIVERSITY";
                break;
        }
        return dealSource;
    }
}