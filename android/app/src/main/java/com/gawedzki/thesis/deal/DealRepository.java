package com.gawedzki.thesis.deal;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;

import com.gawedzki.thesis.R;
import com.gawedzki.thesis.api.RetrofitInstance;

//import org.apache.commons.io.IOUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.SneakyThrows;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DealRepository {

    private DealRemoteDao dealRemoteDao;
    private Context context;
    private String token;
    MutableLiveData<List<Deal>> mAllDeals;
    MutableLiveData<byte[]> mFile = new MutableLiveData<>();

    public MutableLiveData<List<Deal>> getmAllDeals() {
        return mAllDeals;
    }

    public void setmAllDeals(MutableLiveData<List<Deal>> mAllDeals) {
        this.mAllDeals = mAllDeals;
    }

    public MutableLiveData<byte[]> getmFile() {
        return mFile;
    }

    public void setmFile(byte[] bytes) {
        this.mFile.setValue(bytes);
    }

    public DealRepository(Application application) {
        context = application.getApplicationContext();
        mAllDeals = new MutableLiveData<>();
        Retrofit retrofit = RetrofitInstance.getRetrofit();
        dealRemoteDao = retrofit.create(DealRemoteDao.class);
        SharedPreferences sharedPreferences = context.getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token", "00000");
    }

    public void save(DealRequest request) {

        File file = null;
        RequestBody fileBody = null;
        String fileName = null;
        MultipartBody.Part filePart = null;
        try {
            file = new File(request.getFilePath());
            Log.v("tag", "filesize: " + file.length());
            fileBody = RequestBody.create(MediaType.parse("image/jpeg"), file);
            fileName = file.getName();
            filePart = MultipartBody.Part.createFormData("photo", fileName, fileBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestBody budgetNamePart = RequestBody.create(MultipartBody.FORM, request.budgetName);
        RequestBody notePart = RequestBody.create(MultipartBody.FORM, request.note);
        RequestBody valuePart = RequestBody.create(MultipartBody.FORM, request.value.toString());
        RequestBody dealSourcePart = RequestBody.create(MultipartBody.FORM, request.dealSource.toUpperCase());
        RequestBody dealTypePart = RequestBody.create(MultipartBody.FORM, request.dealType.toUpperCase());
        RequestBody datetimePart = RequestBody.create(MultipartBody.FORM, request.datetime.toString());
        RequestBody latitudePart = null;
        RequestBody longitudePart = null;
        if (request.latitude != null && request.longitude != null) {
            latitudePart = RequestBody.create(MultipartBody.FORM, request.latitude.toString());
            longitudePart = RequestBody.create(MultipartBody.FORM, request.longitude.toString());
        }

        Call<Deal> call = dealRemoteDao.post(token, budgetNamePart, notePart, valuePart, dealTypePart, dealSourcePart, datetimePart, latitudePart, longitudePart, filePart);
        call.enqueue(new Callback<Deal>() {
            @Override
            public void onResponse(Call<Deal> call, Response<Deal> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(context, R.string.ops_sth_went_wrong, Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(context, R.string.completed_succesfully, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Deal> call, Throwable t) {
                Toast.makeText(context, R.string.couldnt_connect_with_server, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public MutableLiveData<List<Deal>> getAllDeals() {

        Call<List<Deal>> call = dealRemoteDao.getAllDeals(token);
        call.enqueue(new Callback<List<Deal>>() {
            @Override
            public void onResponse(Call<List<Deal>> call, Response<List<Deal>> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(context, R.string.ops_sth_went_wrong, Toast.LENGTH_SHORT).show();
                    mAllDeals.setValue(response.body());
                    return;
                }
                mAllDeals.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<Deal>> call, Throwable t) {
                Toast.makeText(context, R.string.couldnt_connect_with_server, Toast.LENGTH_SHORT).show();

            }
        });
        return mAllDeals;
    }

    public void delete(Deal deal) {

        Call<Void> call = dealRemoteDao.delete(token, deal.getId());
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(context, R.string.ops_sth_went_wrong, Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(context, R.string.completed_succesfully, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(context, R.string.couldnt_connect_with_server, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void update(DealRequest request) {
        File file = null;
        RequestBody fileBody = null;
        String fileName = null;
        MultipartBody.Part filePart = null;
        try {
            file = new File(request.getFilePath());
            fileBody = RequestBody.create(MediaType.parse("image/jpeg"), file);
            fileName = file.getName();
            filePart = MultipartBody.Part.createFormData("photo", fileName, fileBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestBody budgetIdPart = RequestBody.create(MultipartBody.FORM, request.budgetId.toString());
        RequestBody notePart = RequestBody.create(MultipartBody.FORM, request.note);
        RequestBody valuePart = RequestBody.create(MultipartBody.FORM, request.value.toString());
        RequestBody dealSourcePart = RequestBody.create(MultipartBody.FORM, request.dealSource.toUpperCase());
        RequestBody dealTypePart = RequestBody.create(MultipartBody.FORM, request.dealType.toUpperCase());

        Call<Deal> call = dealRemoteDao.update(token, budgetIdPart, notePart, valuePart, dealTypePart, dealSourcePart, filePart, request.getId());

        call.enqueue(new Callback<Deal>() {
            @Override
            public void onResponse(Call<Deal> call, Response<Deal> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(context, R.string.ops_sth_went_wrong, Toast.LENGTH_SHORT).show();
                    Log.v("tag", String.valueOf(response.code()));
                    return;
                }
                Toast.makeText(context, R.string.completed_succesfully, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<Deal> call, Throwable t) {
                Toast.makeText(context, R.string.couldnt_connect_with_server, Toast.LENGTH_SHORT).show();
                Log.v("tag", String.valueOf(t.getMessage()));
            }
        });
    }

    public void downloadFile(Long dealId) {
        Call<ResponseBody> call = dealRemoteDao.getFile(token, dealId);
        call.enqueue(new Callback<ResponseBody>() {
            @SneakyThrows
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {
                    Toast.makeText(context, R.string.completed_succesfully, Toast.LENGTH_SHORT).show();
                    try {
                        InputStream inputStream = response.body().byteStream();
                        byte[] bytes = getBytesFromInputStream(inputStream);
                        mFile.setValue(bytes);
                        Log.v("tag", "bytes.length: " + bytes.length);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(context, R.string.ops_sth_went_wrong, Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, R.string.couldnt_connect_with_server, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static byte[] getBytesFromInputStream(InputStream is) throws IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        byte[] buffer = new byte[0xFFFF];
        for (int len = is.read(buffer); len != -1; len = is.read(buffer)) {
            os.write(buffer, 0, len);
        }
        return os.toByteArray();
    }
}
