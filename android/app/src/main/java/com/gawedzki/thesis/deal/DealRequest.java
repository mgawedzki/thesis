package com.gawedzki.thesis.deal;

import java.io.File;
import java.time.Instant;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DealRequest {

    public Long id;
    public Long budgetId;
    public String budgetName;
    public String note;
    public String dealType;
    public String dealSource;
    public Instant datetime;
    public Double value;
    public Double latitude;
    public Double longitude;
    public String filePath;
    public File file;

    public DealRequest() {
    }

    public DealRequest(Long id,
                       Long budgetId,
                       String budgetName,
                       String note,
                       String dealType,
                       String dealSource,
                       Instant datetime,
                       Double value,
                       Double latitude,
                       Double longitude,
                       String filePath,
                       File file) {
        this.id = id;
        this.budgetId = budgetId;
        this.budgetName = budgetName;
        this.note = note;
        this.dealType = dealType;
        this.dealSource = dealSource;
        this.datetime = datetime;
        this.value = value;
        this.latitude = latitude;
        this.longitude = longitude;
        this.filePath = filePath;
        this.file = file;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBudgetId() {
        return budgetId;
    }

    public void setBudgetId(Long budgetId) {
        this.budgetId = budgetId;
    }

    public String getBudgetName() {
        return budgetName;
    }

    public void setBudgetName(String budgetName) {
        this.budgetName = budgetName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    public String getDealSource() {
        return dealSource;
    }

    public void setDealSource(String dealSource) {
        this.dealSource = dealSource;
    }

    public Instant getDatetime() {
        return datetime;
    }

    public void setDatetime(Instant datetime) {
        this.datetime = datetime;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
