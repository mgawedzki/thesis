package com.gawedzki.thesis.deal;

import com.gawedzki.thesis.file.FileDto;
import com.gawedzki.thesis.location.Location;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;

import lombok.Data;

@Data
public class DealDto implements Serializable {
    public Long id;
    public long budgetId;
    public String dealType;
    public String dealSource;
    public String note;
    public Float value;
    public Instant datetime;
    public FileDto fileDto;
    public Location location;


}
