package com.gawedzki.thesis.deal;

import java.util.List;
import java.util.Map;

import lombok.Getter;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

public interface DealRemoteDao {

    @Multipart
    @POST("/deals")
//    Call<Deal> post(@Header("Authorization") String token, @Part("photo") MultipartBody.Part file, @Part("photo")RequestBody photo);
    Call<Deal> post(@Header("Authorization") String token,
                    @Part("budgetName") RequestBody budgetName,
                    @Part("note") RequestBody note,
                    @Part("value") RequestBody value,
                    @Part("dealtype") RequestBody dealtype,
                    @Part("dealsource") RequestBody dealsource,
                    @Part("datetime") RequestBody datetime,
                    @Part("latitude") RequestBody latitude,
                    @Part("longitude") RequestBody longitude,
                    @Part MultipartBody.Part photo);

    @GET("/deals")
    Call<List<Deal>> getAllDeals(@Header("Authorization") String token);

    @DELETE("/deals/{id}")
    Call<Void> delete(@Header("Authorization") String token, @Path("id") Long id);

    @Multipart
    @PUT("/deals/{id}")
    Call<Deal> update(@Header("Authorization") String token,
                      @Part("budgetId") RequestBody budgetId,
                      @Part("note") RequestBody note,
                      @Part("value") RequestBody value,
                      @Part("dealtype") RequestBody dealType,
                      @Part("dealsource") RequestBody dealSource,
                      @Part MultipartBody.Part photo,
                      @Path("id") Long id);

    @GET("/deals/files/{id}")
    Call<ResponseBody> getFile(@Header("Authorization") String token, @Path("id") Long id);
}
