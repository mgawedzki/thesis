package com.gawedzki.thesis.menu.management.budgets;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.gawedzki.thesis.R;
import com.gawedzki.thesis.budget.Budget;
import com.gawedzki.thesis.budget.BudgetRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class BudgetAddActivity extends AppCompatActivity {

    private EditText name;
    private EditText description;
    private EditText firstDay;
    private Spinner currencySpinner;
    private ImageView cancel, save;
    private BudgetRepository budgetRepository;

    public boolean anyFieldEmpty() {
        if (name.getText().toString().isEmpty() ||
                firstDay.getText().toString().isEmpty()) {
            Toast.makeText(this, R.string.there_are_empty_fields, Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_budget_add);

        budgetRepository = new BudgetRepository(getApplication());

        name = findViewById(R.id.budget_add_name);
        description = findViewById(R.id.budget_add_description);
        firstDay = findViewById(R.id.budget_add_first_day);
        currencySpinner = findViewById(R.id.budget_add_currency_spinner);
        cancel = findViewById(R.id.budget_add_cancel_button);
        save = findViewById(R.id.budget_add_save_button);

        List<Currency> currencies = new ArrayList<>();
        currencies.addAll(Currency.getAvailableCurrencies());
        List<String> currenciesSymbols = currencies.stream()
                .map(Currency::getCurrencyCode)
                .sorted(String::compareTo)
                .collect(Collectors.toList());

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, currenciesSymbols);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        currencySpinner.setAdapter(adapter);

        setlisteners();
    }

    private void setlisteners() {
        cancel.setOnClickListener(v -> {
            exitActivity();
        });

        save.setOnClickListener(v -> {
            if (anyFieldEmpty()) return;
            saveBudget();
            finish();
        });
    }

    private void saveBudget() {
        Budget budget = new Budget();

        budget.setName(name.getText().toString());
        budget.setDescription(description.getText().toString());
        budget.setFirstDay(Integer.parseInt(firstDay.getText().toString()));
        String currencyCode = currencySpinner.getSelectedItem().toString();
        Currency instance = Currency.getInstance(currencyCode);
        budget.setCurrency(instance);

        budgetRepository.save(budget);
    }

    private void exitActivity() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.exit_confirmation_unsaved_changes)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, (dialog, id) -> {
                    this.finish();
                })
                .setNegativeButton(R.string.no, (dialog, id) -> {
                    dialog.cancel();
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}