package com.gawedzki.thesis.user;

import androidx.lifecycle.LiveData;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface UserRemoteDao {

    @GET("/users/me")
    Call<User> me(@Header("Authorization") String token);

    @GET("/users")
    Call<LiveData<List<User>>> getAllUsers(@Header("Authorization") String token);
}
