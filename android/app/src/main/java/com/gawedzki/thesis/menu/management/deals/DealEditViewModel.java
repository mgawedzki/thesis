package com.gawedzki.thesis.menu.management.deals;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.gawedzki.thesis.deal.Deal;
import com.gawedzki.thesis.deal.DealRepository;
import com.gawedzki.thesis.deal.DealRequest;

import java.util.List;

public class DealEditViewModel extends AndroidViewModel {

    private DealRepository dealRepository;
    private MutableLiveData<List<Deal>> allDeals;

    public DealEditViewModel(@NonNull Application application) {
        super(application);
        dealRepository = new DealRepository(application);
        allDeals = dealRepository.getAllDeals();
    }

    public LiveData<List<Deal>> getAllDeals(){
        return allDeals;
    }

    public void delete(Deal deal) {
        dealRepository.delete(deal);
        List<Deal> value = allDeals.getValue();
        value.remove(deal);
        allDeals.setValue(value);
    }

    public void updateDeals() {
        dealRepository.getAllDeals();
    }
}
