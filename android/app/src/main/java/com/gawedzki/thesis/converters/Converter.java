package com.gawedzki.thesis.converters;

import androidx.room.TypeConverter;

import com.gawedzki.thesis.deal.Deal;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

public class Converter {

    private static Gson gson = new Gson();

    @TypeConverter
    public static List<Deal> stringToDealsList(String data){
        if(data == null){
            return Collections.emptyList();
        }
        Type list = new TypeToken<List<Deal>>(){}.getType();

        return gson.fromJson(data, list);
    }

    @TypeConverter
    public static String dealsListToString(List<Deal> deals){
        return gson.toJson(deals);
    }
}
