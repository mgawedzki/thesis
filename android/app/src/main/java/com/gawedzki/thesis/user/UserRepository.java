package com.gawedzki.thesis.user;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.gawedzki.thesis.api.LocalDatabase;
import com.gawedzki.thesis.api.RetrofitInstance;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class UserRepository {

    private UserLocalDao userLocalDao;
    private UserRemoteDao userRemoteDao;
    private MutableLiveData<List<User>> users = new MutableLiveData<>();
    private MutableLiveData<User> user = new MutableLiveData<>();
    private Context context;

    public UserRepository(Application application) {
        context = application.getApplicationContext();

        //local
//        LocalDatabase database = LocalDatabase.getInstance(application);
//        userLocalDao = database.userDao();

        //remote
        Retrofit retrofit = RetrofitInstance.getRetrofit();
        userRemoteDao = retrofit.create(UserRemoteDao.class);

    }

    public void insert(User user) {
    }

    public void update(User user) {

    }

    public void delete(User user) {

    }

    public MutableLiveData<User> getMe() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE);
        String token = sharedPreferences.getString("token", "00000");

        Call<User> call = userRemoteDao.me(token);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                user.setValue(response.body());
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
            }
        });

        return user;
    }
}
