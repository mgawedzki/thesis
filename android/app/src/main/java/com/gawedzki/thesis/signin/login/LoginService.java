package com.gawedzki.thesis.signin.login;

import com.gawedzki.thesis.signin.login.LoginRequest;
import com.gawedzki.thesis.signin.register.RegistrationRequest;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface LoginService {

    @POST("/login")
    Call<ResponseBody> login(@Body LoginRequest loginRequest);

    @POST("/users/register")
    Call<ResponseBody> register(@Body RegistrationRequest registrationRequest);
}
