package com.gawedzki.thesis;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.widget.ProgressBar;

import com.gawedzki.thesis.home.HomeFragment;
import com.gawedzki.thesis.menu.MenuFragment;
import com.gawedzki.thesis.search.SearchFragment;
import com.gawedzki.thesis.settings.SettingsFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView bottomNavigationView;

    private Fragment homeFragment;
    private Fragment searchFragment;
    private Fragment settingsFragment;
    private Fragment menuFragment;
    private FragmentManager fm;
    private Fragment fragment;
    private ProgressBar progressCircular;
    private LoadingDialog loadingDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
//        NavController navController = Navigation.findNavController(this, R.id.fragment);
//        NavigationUI.setupWithNavController(bottomNavigationView, navController);

        loadingDialog = new LoadingDialog(MainActivity.this);

        homeFragment = new HomeFragment();
        settingsFragment = new SettingsFragment();
        menuFragment = new MenuFragment();
        searchFragment = new SearchFragment();
        fm = getSupportFragmentManager();

        bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);

        fm.beginTransaction().add(R.id.fragment, searchFragment, "1").hide(searchFragment).commit();
        fm.beginTransaction().add(R.id.fragment, homeFragment, "2").show(homeFragment).commit();
        fm.beginTransaction().add(R.id.fragment, menuFragment, "3").hide(menuFragment).commit();
        fm.beginTransaction().add(R.id.fragment, settingsFragment, "4").hide(settingsFragment).commit();

        fragment = homeFragment;
    }

    private BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    FragmentTransaction ft = fm.beginTransaction();
                    switch (item.getItemId()) {
                        case R.id.app_bar_search:
                            ft.hide(fragment).show(searchFragment);
                            fragment = searchFragment;
                            break;
                        case R.id.app_bar_home:
                            ft.hide(fragment).show(homeFragment);
                            fragment = homeFragment;
                            break;
                        case R.id.app_bar_menu:
                            ft.hide(fragment).show(menuFragment);
                            fragment = menuFragment;
                            break;
                        case R.id.app_bar_settings:
                            ft.hide(fragment).show(settingsFragment);
                            fragment = settingsFragment;
                            break;
                    }
                    ft.commit();
                    return true;
                }
            };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public Fragment getHomeFragment() {
        return homeFragment;
    }

    public void setHomeFragment(Fragment homeFragment) {
        fm.beginTransaction().remove(this.homeFragment).commit();
        this.homeFragment = homeFragment;
        fragment = this.homeFragment;
        fm.beginTransaction().add(R.id.fragment, this.homeFragment, "2").show(homeFragment).commit();
    }

    public Fragment getSearchFragment() {
        return searchFragment;
    }

    public void setSearchFragment(Fragment searchFragment) {
        this.searchFragment = searchFragment;
    }

    public Fragment getSettingsFragment() {
        return settingsFragment;
    }

    public void setSettingsFragment(Fragment settingsFragment) {
        this.settingsFragment = settingsFragment;
    }

    public Fragment getMenuFragment() {
        return menuFragment;
    }

    public void setMenuFragment(Fragment menuFragment) {
        this.menuFragment = menuFragment;
    }

    public ProgressBar getProgressCircular() {
        return progressCircular;
    }

    public void setProgressCircular(ProgressBar progressCircular) {
        this.progressCircular = progressCircular;
    }

    public LoadingDialog getLoadingDialog() {
        return loadingDialog;
    }

    public void setLoadingDialog(LoadingDialog loadingDialog) {
        this.loadingDialog = loadingDialog;
    }

    public void showLoadingDialog(){
        loadingDialog.show();
        Handler handler = new Handler();
        handler.postDelayed(() -> loadingDialog.dismiss(), 750);
    }
}