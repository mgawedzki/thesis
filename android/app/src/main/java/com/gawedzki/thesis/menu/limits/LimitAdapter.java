package com.gawedzki.thesis.menu.limits;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.gawedzki.thesis.R;
import com.gawedzki.thesis.limit.Limit;
import com.gawedzki.thesis.limit.LimitType;
import com.gawedzki.thesis.limit.LimitTypeEnum;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Locale;

public class LimitAdapter extends ListAdapter<Limit, LimitAdapter.LimitHolder> {

    private final Context context;
    private List<Limit> limits;

    protected LimitAdapter(Context context, List<Limit> limits) {
        super(DIFF_CALLBACK);
        this.context = context;
        this.limits = limits;
    }

    private static final DiffUtil.ItemCallback<Limit> DIFF_CALLBACK = new DiffUtil.ItemCallback<Limit>() {
        @Override
        public boolean areItemsTheSame(@NonNull Limit oldItem, @NonNull Limit newItem) {
            return oldItem.getId().longValue() == newItem.getId().longValue();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Limit oldItem, @NonNull Limit newItem) {
            boolean same = oldItem.getGoalValue().equals(newItem.getGoalValue()) &&
                    oldItem.getBudget().getId().equals(newItem.getBudget().getId()) &&
                    oldItem.getFrom().equals(newItem.getFrom()) &&
                    oldItem.getUntil().equals(newItem.getUntil()) &&
                    oldItem.getLimitType().getType().equals(newItem.getLimitType().getType())
                    && oldItem.getId().equals(newItem.getId());
            return same;
        }
    };

    public Limit getItemAt(int position) {
        return getItem(position);
    }

    @NonNull
    @Override
    public LimitAdapter.LimitHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.limit_item, parent, false);
        return new LimitHolder(itemView);
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void onBindViewHolder(@NonNull LimitAdapter.LimitHolder holder, int position) {

        SharedPreferences sharedPrefs = context.getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE);
        String locale = sharedPrefs.getString("default_locale", "en");

        Limit currentLimit = getItem(position);
        LimitType type = currentLimit.getLimitType();
        LocalDateTime from = LocalDateTime.ofInstant(currentLimit.getFrom(), ZoneId.systemDefault());
        LocalDateTime until = LocalDateTime.ofInstant(currentLimit.getUntil(), ZoneId.systemDefault());
        float progress;

        String fromStr = from.getDayOfMonth() + "-" +
                from.getMonthValue() + "-" +
                from.getYear();
        String untilStr = until.getDayOfMonth() + "-" +
                until.getMonthValue() + "-" +
                until.getYear();

        if (type.getType().equals(LimitTypeEnum.SAVE_MONEY)) {
            holder.typeTextView.setText(": " + context.getResources().getString(R.string.save_money));
        } else
            holder.typeTextView.setText(": " + context.getResources().getString(R.string.spend_not_more_than));


        if (currentLimit.getLimitType().getType().equals(LimitTypeEnum.SAVE_MONEY)) {
            progress = (currentLimit.getBalance() * 100 / currentLimit.getGoalValue());
            if (progress >= 0 && progress < 100) {
                holder.icon.setImageDrawable(context.getDrawable(R.drawable.ic_arrow_upward_24));
            }
            if (progress >= 100) {
                holder.icon.setImageDrawable(context.getDrawable(R.drawable.ic_done_24));
            }
            if (progress < 0){
                holder.icon.setImageDrawable(context.getDrawable(R.drawable.ic_cross_24));
            }
        } else {
            progress = 100 * (currentLimit.getGoalValue().floatValue() + currentLimit.getBalance()) / currentLimit.getGoalValue();
            if (progress >= 0 && progress < 100) {
                holder.icon.setImageDrawable(context.getDrawable(R.drawable.ic_arrow_downward_24));
            }
            if (progress >= 100) {
                holder.icon.setImageDrawable(context.getDrawable(R.drawable.ic_done_24));
            }
            if (progress < 0) {
                holder.icon.setImageDrawable(context.getDrawable(R.drawable.ic_cross_24));
            }
        }
        String balanceStr = String.valueOf(currentLimit.getBalance());
        if (currentLimit.getBalance() > 0) {
            balanceStr = "+" + currentLimit.getBalance();
        }

        holder.budgetTextView.setText(": " + currentLimit.getBudget().getName());
        holder.goalValueTextView.setText(": " + currentLimit.getGoalValue().toString());
        holder.currentTextView.setText(": " + String.format("%.2f", progress) + "% (" + balanceStr + currentLimit.getBudget().getCurrency().getSymbol(new Locale(locale)) + ")");
        holder.fromTextView.setText(context.getResources().getString(R.string.from) + ": " + fromStr);
        holder.untilTextView.setText(context.getResources().getString(R.string.to) + ": " + untilStr);


    }

    public Limit getLimitAt(int position) {
        return getItem(position);
    }


    class LimitHolder extends RecyclerView.ViewHolder {

        TextView typeTextView, budgetTextView, goalValueTextView, currentTextView, fromTextView, untilTextView;
        ImageView icon;

        public LimitHolder(@NonNull View itemView) {
            super(itemView);
            typeTextView = itemView.findViewById(R.id.limit_item_type);
            budgetTextView = itemView.findViewById(R.id.limit_item_budget);
            goalValueTextView = itemView.findViewById(R.id.limit_item_value);
            currentTextView = itemView.findViewById(R.id.limit_item_current_progress);
            fromTextView = itemView.findViewById(R.id.limit_item_from);
            untilTextView = itemView.findViewById(R.id.limit_item_until);
            icon = itemView.findViewById(R.id.limit_item_icon);
        }
    }
}
