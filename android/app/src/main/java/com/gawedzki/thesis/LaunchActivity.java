package com.gawedzki.thesis;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.gawedzki.thesis.MainActivity;
import com.gawedzki.thesis.R;
import com.gawedzki.thesis.signin.login.LoginActivity;

import java.util.Locale;

public class LaunchActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    private static final String SHARED_PREFS = "sharedPrefs";
    private static final String TOKEN_KEY = "token";

    private TextView noInternet;
    private Button retry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        loadLocale();
        noInternet = findViewById(R.id.no_internet_textview);
        retry = findViewById(R.id.retry_internet_button);
        sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);

        int modeNightFollowSystem = AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM;
        int theme = sharedPreferences.getInt("mytheme", modeNightFollowSystem);
        AppCompatDelegate.setDefaultNightMode(theme);


        if (!isNetworkConnected()) {
            Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();

//            noInternet.setVisibility(View.VISIBLE);
//            retry.setVisibility(View.VISIBLE);

            return;
        }

//        SharedPreferences.Editor edit = sharedPreferences.edit();
//        edit.clear();
//        edit.apply();

        String token = sharedPreferences.getString(TOKEN_KEY, "");
        Intent intent;


        if (token != null && !token.isEmpty() && token.startsWith("Bearer "))
            intent = new Intent(this, MainActivity.class);
        else
            intent = new Intent(this, LoginActivity.class);

        finish();
        startActivity(intent);

    }

    public void startHomeActivity(View view) {

//        SharedPreferences.Editor edit = sharedPreferences.edit();
//        edit.clear();
//        edit.apply();

        if (!isNetworkConnected()) {
            Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();
            return;
        }

        String token = sharedPreferences.getString(TOKEN_KEY, "");
        Intent intent;


        if (token != null && !token.isEmpty() && token.startsWith("Bearer "))
            intent = new Intent(this, MainActivity.class);
        else
            intent = new Intent(this, LoginActivity.class);

        finish();
        startActivity(intent);
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    private void setlocale(String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        SharedPreferences.Editor editor = getSharedPreferences("sharedPrefs", MODE_PRIVATE).edit();
        editor.putString("default_locale", lang);
        editor.apply();
    }

    public void loadLocale() {
        SharedPreferences sharedPrefs = getSharedPreferences("sharedPrefs", MODE_PRIVATE);
        String s = sharedPrefs.getString("default_locale", "en");
        setlocale(s);
    }
}