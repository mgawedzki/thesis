package com.gawedzki.thesis.menu.limits;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gawedzki.thesis.R;
import com.gawedzki.thesis.budget.Budget;
import com.gawedzki.thesis.limit.Limit;
import com.gawedzki.thesis.limit.LimitType;
import com.gawedzki.thesis.limit.LimitTypeEnum;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.stream.Collectors;

public class LimitsActivity extends AppCompatActivity {

    RadioButton saveMoneyRadio, spendNoMoreThanRadio;
    EditText valueEditText;
    Spinner budgetSpinner;
    LimitsViewModel viewModel;
    RecyclerView recyclerView;
    LimitAdapter adapter;
    CheckBox fromFirstDayCheckBox, untilFirstDayCheckBox;
    TextView fromDatePicker, untilDatePicker;
    private Budget selectedBudget;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_limits);
        initViews();
        viewModel = ViewModelProviders.of(this).get(LimitsViewModel.class);
        setViewModelListeners();
        setViewListeners();
    }

    private void setViewListeners() {
        fromFirstDayCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                fromDatePicker.setVisibility(View.GONE);
                fromDatePicker.setText("Od");

            } else {
                fromDatePicker.setVisibility(View.VISIBLE);
            }
        });

        untilFirstDayCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                untilDatePicker.setVisibility(View.GONE);
                untilDatePicker.setText("Do");
            } else {
                untilDatePicker.setVisibility(View.VISIBLE);
            }
        });

        fromDatePicker.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dialog = new DatePickerDialog(this, (view, year12, month12, dayOfMonth12) ->
                    fromDatePicker.setText(LocalDate.of(year12, month12 + 1, dayOfMonth12).toString()), year, month, dayOfMonth);
            dialog.show();
        });

        untilDatePicker.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dialog = new DatePickerDialog(this, (view, year1, month1, dayOfMonth1) ->
                    untilDatePicker.setText(LocalDate.of(year1, month1 + 1, dayOfMonth1).toString()), year, month, dayOfMonth);
            dialog.show();
        });

        findViewById(R.id.limit_save).setOnClickListener(v -> {
            try {
                saveLimit();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });
    }

    private void saveLimit() throws ParseException {

        Limit limit;
        long limitTypeId;
        float value;
        Budget budget;
        Instant from = Instant.now();
        Instant until = Instant.now();

        if (valueEditText.getText().toString().isEmpty()) {
            Toast.makeText(this, R.string.there_are_empty_fields, Toast.LENGTH_SHORT).show();
            return;
        }

        limit = new Limit();
        limitTypeId = saveMoneyRadio.isChecked() ? 1L : 2L;
        value = Float.parseFloat(valueEditText.getText().toString());
        int selectedItemPosition = budgetSpinner.getSelectedItemPosition();
        budget = viewModel.getmBudgets().getValue().get(selectedItemPosition);

        if (!fromFirstDayCheckBox.isChecked()) {
            String timeString = "00:00";
            String fromDateString = fromDatePicker.getText().toString();
            from = resolveDateTimeStringToInstant(fromDateString, timeString);
        } else {
            LocalDate now = LocalDate.now();
            int firstDay = viewModel.getmBudgets().getValue().get(budgetSpinner.getSelectedItemPosition()).getFirstDay();
            String dateStr = now.getYear() + "-" + now.getMonthValue() + "-" + firstDay;
            from = new SimpleDateFormat("yyyy-MM-dd").parse(dateStr).toInstant();
        }

        if (!fromFirstDayCheckBox.isChecked()) {
            String timeString = "00:00";
            String untilDateString = untilDatePicker.getText().toString();
            until = resolveDateTimeStringToInstant(untilDateString, timeString);
        } else {
            LocalDate now = LocalDate.now();
            int firstDay = viewModel.getmBudgets().getValue().get(budgetSpinner.getSelectedItemPosition()).getFirstDay();
            int monthValue = now.getMonthValue() + 1;
            int year = now.getYear();
            if (monthValue == 12) {
                monthValue = 1;
                year++;
            }
            String dateStr = year + "-" + monthValue + "-" + firstDay;
            until = new SimpleDateFormat("yyyy-MM-dd").parse(dateStr).toInstant();
        }

        if (saveMoneyRadio.isChecked()) limitTypeId = 1;
        else limitTypeId = 2;

        limit.setFrom(from);
        limit.setUntil(until);
        limit.setGoalValue(value);
        limit.setBudget(budget);
        limit.setLimitType(new LimitType(limitTypeId, LimitTypeEnum.SAVE_MONEY));

        viewModel.save(limit);
    }

    private Instant resolveDateTimeStringToInstant(String dateString, String timeString) {
        LocalDate localDate = LocalDate.parse(dateString);
        LocalTime localTime = LocalTime.parse(timeString, DateTimeFormatter.ofPattern("HH:mm"));
        LocalDateTime localDateTime = LocalDateTime.of(localDate, localTime);
        Instant instant = localDateTime.atZone(ZoneId.systemDefault()).toInstant();
        return instant;
    }


    private void initViews() {
        recyclerView = findViewById(R.id.limit_recycler);
        budgetSpinner = findViewById(R.id.limits_budget_spinner);
        fromFirstDayCheckBox = findViewById(R.id.limit_from_firstday_checkbox);
        untilFirstDayCheckBox = findViewById(R.id.limit_until_firstday_checkbox);
        saveMoneyRadio = findViewById(R.id.limit_save_money_radio);
        spendNoMoreThanRadio = findViewById(R.id.limit_spend_no_more_than_radio);
        valueEditText = findViewById(R.id.limit_value_edittext);
        fromDatePicker = findViewById(R.id.limit_date_picker_from);
        untilDatePicker = findViewById(R.id.limit_date_picker_until);

        setupDatePickers();

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                delete(viewHolder, direction, adapter);
            }
        }).attachToRecyclerView(recyclerView);
    }

    private void delete(RecyclerView.ViewHolder viewHolder, int direction, LimitAdapter adapter) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_q)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, (dialog, id) -> {
                    viewModel.delete(adapter.getLimitAt(viewHolder.getAdapterPosition()));
                    adapter.notifyItemRemoved(viewHolder.getAdapterPosition());
                })
                .setNegativeButton(R.string.no, (dialog, id) -> {
                    dialog.cancel();
                    adapter.notifyItemChanged(viewHolder.getAdapterPosition());
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void setupDatePickers() {
        LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.now(), ZoneId.systemDefault());
        String dateStr = localDateTime.getDayOfMonth() + "-" +
                localDateTime.getMonthValue() + "-" +
                localDateTime.getYear();

        fromDatePicker.setText(dateStr);
        untilDatePicker.setText(dateStr);
    }

    private void setViewModelListeners() {
        adapter = new LimitAdapter(this, viewModel.getmLimits().getValue());
        viewModel.getmLimits().observe(this, limits -> {
            adapter.submitList(viewModel.getmLimits().getValue());
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(adapter);
        });
        viewModel.updateLimits();

        viewModel.getmBudgets().observe(this, budgets -> {
            ArrayAdapter<String> budgetAdapter = new ArrayAdapter<String>(
                    this,
                    R.layout.support_simple_spinner_dropdown_item,
                    viewModel.getmBudgets().getValue().stream().map(Budget::getName).collect(Collectors.toList()));
            budgetSpinner.setAdapter(budgetAdapter);
        });
        viewModel.updateBudgets();
    }
}