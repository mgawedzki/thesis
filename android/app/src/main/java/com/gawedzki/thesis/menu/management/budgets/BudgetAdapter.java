package com.gawedzki.thesis.menu.management.budgets;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.gawedzki.thesis.R;
import com.gawedzki.thesis.budget.Budget;
import com.gawedzki.thesis.deal.Deal;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class BudgetAdapter extends ListAdapter<Budget, BudgetAdapter.BudgetHolder> implements Filterable {

    private final Context context;
    private final List<Budget> budgets;
    private List<Budget> filteredBudgets;

    public BudgetAdapter(Context context, List<Budget> budgets) {
        super(DIFF_CALLBACK);
        this.context = context;
        this.budgets = budgets;
        if (budgets != null)
            this.filteredBudgets = new ArrayList<>(budgets);
    }

    private static final DiffUtil.ItemCallback<Budget> DIFF_CALLBACK = new DiffUtil.ItemCallback<Budget>() {
        @Override
        public boolean areItemsTheSame(@NonNull Budget oldItem, @NonNull Budget newItem) {
            return oldItem.getId().longValue() == newItem.getId().longValue();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Budget oldItem, @NonNull Budget newItem) {
            boolean b = false;

            if (oldItem.getName().equals(newItem.getName()) &&
                    oldItem.getDescription().equals(newItem.getDescription()) &&
                    oldItem.getFirstDay() == newItem.getFirstDay() &&
                    oldItem.getDeals().size() == newItem.getDeals().size()) {
                b = true;
            }

            if (oldItem.getCurrency() != null && newItem.getCurrency() != null) {
                if (oldItem.getCurrency().getCurrencyCode().equals(newItem.getCurrency().getCurrencyCode()))
                    b = true;
            }
            return b;
        }
    };


    public Budget getBudgetAt(int position) {
        return getItem(position);
    }

    @NonNull
    @Override
    public BudgetHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.budget_item, parent, false);
        return new BudgetHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BudgetHolder holder, int position) {
        Budget currentBudget = getItem(position);

        int dealsCount = currentBudget.getDeals().size();
        double sum = currentBudget.getDeals().stream().mapToDouble(Deal::getValue).sum();

        holder.id.setText(String.valueOf(currentBudget.getId()));
        holder.budgetName.setText(currentBudget.getName());
        holder.dealsCount.setText(String.valueOf(dealsCount));
        holder.firstDay.setText(String.valueOf(currentBudget.getFirstDay()));
        if (currentBudget.getDescription() != null || currentBudget.getDescription().isEmpty()) {
            holder.description.setText(currentBudget.getDescription());
        }
        holder.value.setText(String.valueOf(sum));
        if (currentBudget.getCurrency() != null) {
            SharedPreferences sharedPrefs = context.getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE);
            String locale = sharedPrefs.getString("default_locale", "en");
            holder.currency.setText(" (" + currentBudget.getCurrency().getSymbol(new Locale(locale)) + ")");
        }

    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Budget> filtered;

            if (constraint == null || constraint.length() == 0) {
                filtered = new ArrayList<>(budgets);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                filtered = filteredBudgets.stream().filter(budget -> budget.getName().toLowerCase().trim().contains(filterPattern)).collect(Collectors.toList());
            }

            FilterResults results = new FilterResults();
            results.values = filtered;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
//            filteredBudgets.clear();
//            filteredBudgets.addAll((List) results.values);
            budgets.clear();
            budgets.addAll((List) results.values);

            notifyDataSetChanged();
        }
    };

    class BudgetHolder extends RecyclerView.ViewHolder {

        private TextView id;
        private TextView budgetName;
        private TextView value;
        private TextView currency;
        private TextView firstDay;
        private TextView dealsCount;
        private TextView description;

        public BudgetHolder(@NonNull View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.budget_item_id);
            budgetName = itemView.findViewById(R.id.budget_item_name);
            value = itemView.findViewById(R.id.budget_item_value);
            currency = itemView.findViewById(R.id.budget_item_currency);
            firstDay = itemView.findViewById(R.id.budget_item_first_day);
            dealsCount = itemView.findViewById(R.id.budget_item_deals_count);
            description = itemView.findViewById(R.id.budget_item_description);
        }
    }

}
