package com.gawedzki.thesis.menu.management.budgets;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.gawedzki.thesis.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class BudgetsManagement extends AppCompatActivity {

    private BudgetEditViewModel budgetEditViewModel;
    private ImageButton refresh;
    private FloatingActionButton add;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_budgets_management);
        refresh = findViewById(R.id.budget_list_refresh);
        add = findViewById(R.id.budget_list_add);
        budgetEditViewModel = ViewModelProviders.of(this).get(BudgetEditViewModel.class);

        RecyclerView recyclerView = findViewById(R.id.budgets_recycler);
        TextView info = findViewById(R.id.budget_list_info);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        budgetEditViewModel.updateBudgets();
        BudgetAdapter adapter = new BudgetAdapter(getApplicationContext(), budgetEditViewModel.getAllBudgets().getValue());
        recyclerView.setAdapter(adapter);

        budgetEditViewModel.getAllBudgets().observe(this, budgets -> {
            if (budgets.size() == 0) {
                info.setText(R.string.empty);
                info.setVisibility(View.VISIBLE);
            } else {
                adapter.submitList(budgets);
                info.setVisibility(View.GONE);
            }
        });
        budgetEditViewModel.getAllBudgets();

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                if (budgetEditViewModel.getAllBudgets().getValue().size() == 1){
                    Toast.makeText(BudgetsManagement.this, R.string.cant_delete_last_budget, Toast.LENGTH_SHORT).show();
                    adapter.notifyItemChanged(viewHolder.getAdapterPosition());
                    return;
                }
                showAlertDialog(viewHolder, direction, adapter);
            }
        }).attachToRecyclerView(recyclerView);

        refresh.setOnClickListener(v -> {
//            this.recreate();
            budgetEditViewModel.updateBudgets();
            Toast.makeText(this, R.string.updating, Toast.LENGTH_SHORT).show();
        });

        add.setOnClickListener(v -> {
            Intent intent = new Intent(this, BudgetAddActivity.class);
            startActivity(intent);
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        budgetEditViewModel.updateBudgets();
    }

    private void showAlertDialog(RecyclerView.ViewHolder viewHolder, int direction, BudgetAdapter adapter) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_q)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, (dialog, id) -> {
                    budgetEditViewModel.delete(adapter.getBudgetAt(viewHolder.getAdapterPosition()));
                    adapter.notifyItemRemoved(viewHolder.getAdapterPosition());
                })
                .setNegativeButton(R.string.no, (dialog, id) -> {
                    dialog.cancel();
                    adapter.notifyItemChanged(viewHolder.getAdapterPosition());
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}