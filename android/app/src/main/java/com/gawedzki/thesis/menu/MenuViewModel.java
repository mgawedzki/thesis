package com.gawedzki.thesis.menu;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.gawedzki.thesis.user.User;
import com.gawedzki.thesis.user.UserRepository;

public class MenuViewModel extends AndroidViewModel {

    private UserRepository userRepository;
    private MutableLiveData<User> user;

    public MenuViewModel(@NonNull Application application) {
        super(application);
        userRepository = new UserRepository(application);
        user = userRepository.getMe();
    }

    public LiveData<User> getMe(){
        return user;
    }
}
