package com.gawedzki.thesis.signin.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.gawedzki.thesis.MainActivity;
import com.gawedzki.thesis.R;
import com.gawedzki.thesis.api.RetrofitInstance;
import com.gawedzki.thesis.signin.register.RegistrationActivity;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    EditText username;
    EditText password;
    Button loginBtn;
    TextView registerBtn, forgotPasswordBtn;
    private static final String SHARED_PREFS = "sharedPrefs";
    private static final String TOKEN_KEY = "token";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        initViews();
    }

    private void initViews() {
        username = findViewById(R.id.inputUsername);
        password = findViewById(R.id.inputPassword);
        loginBtn = findViewById(R.id.loginButton);
        registerBtn = findViewById(R.id.register_btn);
        forgotPasswordBtn = findViewById(R.id.forgot_password_btn);
    }

    public void login(View view) {
        LoginService loginService = RetrofitInstance.getRetrofit().create(LoginService.class);

        String username = this.username.getText().toString();
        String password = this.password.getText().toString();
        LoginRequest loginRequest = new LoginRequest(username, password);

        Call<ResponseBody> loginApiCall = loginService.login(loginRequest);
        loginApiCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                String token = response.headers().get("Authorization");

                if (token != null && !token.isEmpty() && token.startsWith("Bearer ")) {
                    Toast.makeText(getBaseContext(), R.string.logging_in, Toast.LENGTH_SHORT).show();

                    SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(TOKEN_KEY, token);
                    editor.apply();

                    Intent intent = new Intent(getBaseContext(), MainActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    finish();

                } else {
                    Toast.makeText(getBaseContext(), R.string.wrong_credentials, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getBaseContext(), "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void register(View view){
        Intent registerIntent = new Intent(this, RegistrationActivity.class);
        startActivity(registerIntent);
    }
}